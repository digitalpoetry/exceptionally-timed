<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><?php if ( $user_group === 'agent' ) : ?>

    <div class="jumbotron">
        <h1><?= $total ?> Time Exceptions</h1>
        <p>You have <?= $total ?> Unprocessed time exceptions.</p>
        <p><a class="btn btn-primary btn-lg" href="<?= site_url('/admin/texceptions') ?>" role="button">View Your Exceptions »</a></p>
    </div><!-- /.jumbotron -->

    <div class="row">
        <div class="col-md-4">
            <h2><?= $pending ?> Pending</h2>
            <p>You have <?= $pending ?> pending time exceptions.</p>
        </div>
        <div class="col-md-4">
            <h2><?= $approved ?> Approved</h2>
            <p>You have <?= $approved ?> approved time exceptions.</p>
        </div>
        <div class="col-md-4">
            <h2><?= $denied ?> Denied</h2>
            <p>You have <?= $denied ?> denied time exceptions.</p>
        </div>
    </div><!-- /.row -->

<?php elseif ( $user_group === 'supervisor' ) : ?>

    <div class="jumbotron">
        <h1><?= $unprocessed ?> Time Exceptions</h1>
        <p>You have <?= $unprocessed ?> time exceptions needing your attention!</p>
        <p><a class="btn btn-primary btn-lg" href="<?= site_url('/admin/texceptions/review') ?>" role="button">Review Exceptions »</a></p>
    </div><!-- /.jumbotron -->

    <div class="row">
        <div class="col-md-4">
            <h2><?= $reviewed ?> Reviewed</h2>
            <p>You have <?= $reviewed ?> reviewed time exceptions.</p>
        </div>
        <div class="col-md-4">
            <h2><?= $unreviewed ?> Unreviewed</h2>
            <p>You have <?= $unreviewed ?> unreviewed time exceptions.</p>
        </div>
        <div class="col-md-4">
            <h2><?= $unprocessed ?> Unprocessed</h2>
            <p>You have <?= $unprocessed ?> unprocessed time exceptions.</p>
        </div>
    </div><!-- /.row -->

<?php endif ?>