<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="<?= site_url('admin/texceptions') ?>" class="btn btn-default" role="button">Cancel</a>
    </div>
    <h2 >New Time Exception <small>Do it meow!</small></h2>
</div>

<?= form_open('', array('class' => 'form-horizontal')); ?>

    <div class="row">

        <!-- Supervisor -->
        <div class="form-group">
            <label for="supervisor" class="col-md-2 control-label">Supervisor</label>
            <div class="col-md-3">
                <?= form_dropdown('supervisor', $supervisor_options, $supervisor, ['class' => 'form-control']) ?>
            </div>
        </div>

        <!-- Start -->
        <div class="form-group">
            <label class="col-md-2 control-label" for="start">Start</label>
            <div class="col-md-3">
                <input type="datetime-local" class="form-control" id="start" name="start" step="60" value="<?= set_value('start', $start_default) ?>" />
            </div>
        </div>

        <!-- End -->
        <div class="form-group">
            <label for="end" class="col-md-2 control-label">End</label>
            <div class="col-md-3">
                <input type="datetime-local" class="form-control" id="end" name="end" step="60" value="<?= set_value('end', $end_default) ?>" />
            </div>
        </div>

        <!-- Authorized By -->
        <div class="form-group">
            <label for="authorized_by" class="col-md-2 control-label">Authorized By</label>
            <div class="col-md-3">
                <?php array_unshift($supervisor_options, '') ?>
                <?= form_dropdown('authorized_by', $supervisor_options, '', ['class' => 'form-control']) ?>
            </div>
        </div>

        <!-- Snippet -->
        <div class="form-group">
            <div class="col-md-offset-2 col-md-3">
                <select class="form-control" id="snippet" name="snippet" onchange="$('#reason').val( this.value ); $('#snippet').val('select');">
                    <option hidden="" value="select">use a snippet...</option>
                    <?php foreach ($snippets as $option) : ?>
                    <option value="<?= $option ?>"><?= $option ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <!-- Reason -->
        <div class="form-group">
            <label for="reason" class="col-md-2 control-label">Reason</label>
            <div class="col-md-6">
                <input type="text" class="form-control" id="reason" name="reason" value="<?= set_value('reason', '' ) ?>" placeholder="<?= lang('form_reason_placeholder') ?>" />
            </div>
        </div>

        <!-- Submit -->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-primary" name="submit" value="Submit Exception" />
            </div>
        </div>

    </div>

<?= form_close(); ?>
