<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="<?= site_url('admin/texceptions/create') ?>" class="btn btn-primary" role="button">New Exception</a>
    </div>
    <h2 >Time Exceptions <small>You're behind the curtain now Dorothy...</small></h2>
</div>

<?php if ( ! empty($texceptions) && is_array($texceptions) && count($texceptions) ) : ?>

    <div class="table table-responsive">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>Agent</th>
                    <th>Supervisor</th>
                    <th>Authorized By</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Reason</th>
                    <th>Approved</th>
                    <th>Submitted On</th>
                    <th class="col-sm-2">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($texceptions as $texception) : ?>
                <tr>
                    <td><?= get_user_fullname($texception->created_by) ?></td>
                    <td><?= get_user_fullname($texception->supervisor) ?></td>
                    <td><?= get_user_fullname($texception->authorized_by) ?></td>
                    <td><?= datetime_to_str($texception->start) ?></td>
                    <td><?= datetime_to_str($texception->end) ?></td>
                    <td><?= $texception->reason ?></td>
                    <td><?= bool_to_str($texception->approved) ?></td>
                    <td><?= datetime_to_str($texception->created_on) ?></td>
                    <td class="table-actions">
                        <a class="text-primary" href="<?= site_url('admin/texceptions/show/' . $texception->id)   ?>">View</a> |
                        <a class="text-info" href="<?= site_url('admin/texceptions/update/' . $texception->id) ?>">Edit</a> |
                        <a class="text-danger" href="<?= site_url('admin/texceptions/delete/' . $texception->id) ?>" onclick="return confirm('Delete this item?');">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <nav class="pull-right">
            <?= html_entity_decode($pagination) ?>
        </nav>    
    </div>

<?php else : ?>

    <div class="alert alert-warning">
        Unable to find any records. <a href="#" class="close">&times;</a>
    </div>

<?php endif; ?>
