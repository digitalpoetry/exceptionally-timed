<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="<?= site_url('admin/texceptions/update/' . $texception->id) ?>" class="btn btn-primary" role="button">Edit Exception</a>
        <a href="<?= site_url('admin/texceptions/delete/' . $texception->id) ?>" class="btn btn-danger" role="button" onclick="return confirm('Delete this item?');">Delete Exception</a>
        <a href="<?= site_url('admin/texceptions') ?>" class="btn btn-default" role="button">Cancel</a>
    </div>
    <h2 >Time Exception <small>Is it everything you wished for?</small></h2>
</div>

<table class="table table-striped table-condensed">
    <tbody>
        <tr>
            <th class="col-sm-2">Created By</th>
            <td><?= get_user_fullname($texception->created_by) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Supervisor</th>
            <td><?= get_user_fullname($texception->supervisor) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Authorized By</th>
            <td><?= get_user_fullname($texception->authorized_by) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Start</th>
            <td><?= datetime_to_text($texception->start) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">End</th>
            <td><?= datetime_to_text($texception->end) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Reason</th>
            <td><?= $texception->reason ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Approved</th>
            <td><?= bool_to_str($texception->approved) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Approved By</th>
            <td><?= get_user_fullname($texception->approved_by) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Approved On</th>
            <td><?= datetime_to_text($texception->approved_on) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Submitted On</th>
            <td><?= datetime_to_text($texception->created_on) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Modified On</th>
            <td><?= datetime_to_text($texception->modified_on) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Deleted</th>
            <td><?= bool_to_str($texception->deleted) ?></td>
        </tr>
        <tr>
            <th class="col-sm-2">Deleted By</th>
            <td><?= get_user_fullname($texception->deleted_by) ?></td>
        </tr>
    </tbody>
</table>
