<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><div id="container">

    <div class="jumbotron">
        <h1>Exceptionally Timed!</h1>
        <p>Easily submit and manage your time exceptions...</p>
        <?php if ( empty($_SESSION['logged_in']) ) : ?>
        <p><a class="btn btn-primary btn-lg" href="<?= site_url('/login') ?>" role="button">Log in</a></p>
        <?php else : ?>
        <p><?= html_entity_decode($jumbo_button) ?></p>
        <?php endif ?>
    </div>

</div>
