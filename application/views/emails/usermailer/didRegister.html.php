<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><table class="row">
    <tr>
        <td>
            <h1>Your New Here, Right?</h1>

            <p>Hey there!</p>

            <p>Someone using this email (<?= $email ?>) just signed up for an account at <a href="<?= $site_link ?>"><?= $site_name ?></a>. If that was not you, then disregard these instructions
                and all will be well.</p>

            <p>If that was you - then click the link below to activate your account:</p>

            <p>
                <a href="<?= $link ."?e={$email}&code={$token}" ?>">
                    <?= $link ."?e={$email}&code={$token}" ?>
                </a>
            </p>

            <p>If the link does not work, please visit the following page: <b><?= $link ?></b> and enter the following token when asked:</p>

            <p><?= $token ?></p>

            <p>Thanks!<br/><?= $site_name ?></p>
        </td>
    </tr>
</table>
