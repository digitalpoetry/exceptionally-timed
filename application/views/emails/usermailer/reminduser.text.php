<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?>Hey there!

Someone using this email (<?= $email ?>) just requested password reset instructions. If that was not you, then disregard these instructions
and all will be well.

If you do need to reset your password, please visit the following link:

<?= $link ."?e={$email}&code={$code}" ?>

If the link does not work, please visit the following page: <?= $link ?> and enter the following code when asked:

<?= $code ?>

Thanks!
<?= $site_name ?>