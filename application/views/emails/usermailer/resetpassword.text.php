<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?>Hey there!

Someone using this email (<?= $email ?>) just reset the account password. If that was you, then disregard these instructions
and all will be well.

If you did not do this, then you should reset your password immediately by visiting the following link, and clicking the Forgot Your Password link:

<?= $link ?>

If the link does not work, please visit the following page: <?= $link ?>

Thanks!
<?= $site_name ?>