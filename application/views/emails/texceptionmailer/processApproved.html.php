<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><table class="row">
    <tr>
        <td>
            <h1>Approved Time Exceptions</h1>

            <p>Below is a list if approved time exceptions for your records:</p>
            
            <table>
                <thead>
                    <tr>
                        <th>Created By</th>
                        <th>Authorized By</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Reason</th>
                        <th>Supervisor</th>
                        <th>Submitted On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($texceptions as $texception) : ?>
                    <tr>
                        <td><?= get_user_fullname($texception->created_by) ?></td>
                        <td><?= get_user_fullname($texception->authorized_by) ?></td>
                        <td><?= datetime_to_text($texception->start) ?></td>
                        <td><?= datetime_to_text($texception->end) ?></td>
                        <td><?= $texception->reason ?></td>
                        <td><?= get_user_fullname($texception->supervisor) ?></td>
                        <td><?= datetime_to_text($texception->created_on) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <p>Thanks!<br/><a href="<?= $site_link ?>"><?= $site_name ?></a></p>
        </td>
    </tr>
</table>
