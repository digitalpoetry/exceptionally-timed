<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?>Approved Time Exceptions

Below is a list if approved time exceptions for your records:
            
<?php 
    foreach ($texceptions as $texception)
    {
        echo "\n",
        "Created By:    ", get_user_fullname($texception->created_by) .    "\n",
        "Authorized By: ", get_user_fullname($texception->authorized_by) . "\n",
        "Start:         ", datetime_to_text($texception->start) .          "\n",
        "End:           ", datetime_to_text($texception->end) .            "\n",
        "Reason:        ", $texception->reason  .                          "\n",
        "Supervisor:    ", get_user_fullname($texception->supervisor) .    "\n",
        "Submitted On:  ", datetime_to_text($texception->created_on) .     "\n";
    }
?>

Thanks!
<?= $site_name ?>
<?= $site_link ?>
