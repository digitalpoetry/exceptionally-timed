<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><table class="row">
    <tr>
        <td>
            <h1>Denied Time Exception</h1>

            <p>The following time exception was denied:</p>
            
            <table>
                <tbody>
                    <tr>
                        <th>Created By</th>
                        <td><?= get_user_fullname($texception->created_by) ?></td>
                    </tr>
                    <tr>
                        <th>Authorized By</th>
                        <td><?= get_user_fullname($texception->authorized_by) ?></td>
                    </tr>
                    <tr>
                        <th>Start</th>
                        <td><?= datetime_to_text($texception->start) ?></td>
                    </tr>
                    <tr>
                        <th>End</th>
                        <td><?= datetime_to_text($texception->end) ?></td>
                    </tr>
                    <tr>
                        <th>Reason</th>
                        <td><?= $texception->reason ?></td>
                    </tr>
                    <tr>
                        <th>Supervisor</th>
                        <td><?= get_user_fullname($texception->supervisor) ?></td>
                    </tr>
                    <tr>
                        <th>Submitted On</th>
                        <td><?= datetime_to_text($texception->created_on) ?></td>
                    </tr>
                </tbody>
            </table>

            <p>Thanks!<br/><a href="<?= $site_link ?>"><?= $site_name ?></a></p>
        </td>
    </tr>
</table>
