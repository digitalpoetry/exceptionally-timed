<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="<?= site_url('admin/users') ?>" class="btn btn-default" role="button">Cancel</a>
    </div>
    <h2 >Create User <small>Another newb...</small></h2>
</div>

<?= form_open('', array('class' => 'form-horizontal')); ?>

    <div class="row">

        <?= $notice ?>

        <!-- First name -->
        <div class="form-group">
            <label for="first_name" class="col-md-2 control-label">First name</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="first_name" name="first_name" value="<?= set_value('first_name') ?>" />
            </div>
        </div>

        <!-- Last name -->
        <div class="form-group">
            <label for="last_name" class="col-md-2 control-label">Last name</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="last_name" name="last_name" value="<?= set_value('last_name') ?>" />
            </div>
        </div>

        <!-- Email -->
        <div class="form-group">
            <label for="email" class="col-md-2 control-label">Email</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="email" name="email" value="<?= set_value('email') ?>" />
            </div>
        </div>

        <!-- Username -->
        <div class="form-group">
            <label for="username" class="col-md-2 control-label"><?= lang('auth.username') ?></label>
            <div class="col-md-3">
                <input type="text" name="username" class="form-control" required="" value="<?= set_value('username') ?>">
            </div>
        </div>

        <!-- Password -->
        <div class="form-group">
            <label for="password" class="col-md-2 control-label">New <?= lang('auth.password') ?></label>
            <div class="col-md-3">
                <input type="password" name="password" id="password" class="form-control" required="">
                <?= $uikit->notice(lang('auth.password_strength'), 'default', false, ['class' => 'pass-strength alert-info']); ?>
            </div>
        </div>

        <!-- Confirm Pass -->
        <div class="form-group">
            <label for="pass_confirm" class="col-md-2 control-label"><?= lang('auth.pass_confirm') ?></label>
            <div class="col-md-3">
                <input type="password" name="pass_confirm" id="pass-confirm" class="form-control" required="">
            </div>
        </div>

        <!-- User Group -->
        <div class="form-group">
            <label for="user_group" class="col-md-2 control-label">User Group</label>
            <div class="col-md-3">
                <?= form_dropdown('user_group', $user_group_options, set_value('user_group', [config_item('auth.default_role_id')]), ['class' => 'form-control']) ?>
            </div>
        </div>

        <!-- Submit -->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-primary" id="submit" name="submit" disabled value="<?= lang('auth.create_account') ?>" />
            </div>
        </div>

    </div>

<?= form_close(); ?>
