<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="<?= site_url('admin/users/create') ?>" class="btn btn-primary" role="button"><?= lang('auth.create_account') ?></a>
    </div>
    <h2 >Users <small>Too bad this ain't your friends list...</small></h2>
</div>

<?php if ( ! empty($users) && is_array($users) && count($users) ) : ?>

    <div class="table table-responsive">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Group</th>
                    <th class="col-sm-2">Actions</th>
                </tr>
            </thead>
            <tbody>
            
                <?php foreach ($users as $user) : ?>
                <tr>
                    <td><?= $user->username ?></td>
                    <td><?= $user->email ?></td>
                    <td><?= $user->meta->first_name ?></td>
                    <td><?= $user->meta->last_name ?></td>
                    <td><?= $user->description ?></td>
                    <td class="table-actions">
                        <a href="<?= site_url('admin/users/show/' . $user->id)   ?>" class="text-info">View</a> |
                        <a href="<?= site_url('admin/users/update/' . $user->id) ?>" class="text-primary">Edit</a> |
                        <a href="<?= site_url('admin/users/delete/' . $user->id) ?>" class="text-danger" onclick="return confirm('Delete this user?');">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
        <nav class="pull-right">
            <?= html_entity_decode($pagination) ?>
        </nav>    
    </div><!-- /.table-responsive -->

<?php else : ?>

    <div class="alert alert-warning">
        Unable to find any users. <a href="#" class="close">&times;</a>
    </div>

<?php endif; ?>
