<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="<?= site_url('admin/users/force_reset/' . $user->id) ?>" class="btn btn-info" role="button" onclick="return confirm('Force user to reset password?');">Force Password Reset</a>
        <a href="<?= site_url('admin/users/delete/' . $user->id) ?>" class="btn btn-danger" role="button" onclick="return confirm('Delete this user?');">Delete User</a>
        <a href="<?= site_url('admin/users/show/' . $user->id) ?>" class="btn btn-default" role="button">Cancel</a>
    </div>
    <h2 >Update User</h2>
</div>

<?= form_open('', array('class' => 'form-horizontal')); ?>

    <div class="row">

        <!-- Email -->
        <div class="form-group">
            <label for="email" class="col-md-2 control-label">Email</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="email" name="email" value="<?= set_value('email', $user->email ) ?>" placeholder="<?= lang('form_email_placeholder') ?>" />
            </div>
        </div>

        <!-- First name -->
        <div class="form-group">
            <label for="first_name" class="col-md-2 control-label">First name</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="first_name" name="first_name" value="<?= set_value('first_name', $user->meta->first_name ) ?>" placeholder="<?= lang('form_first name_placeholder') ?>" />
            </div>
        </div>

        <!-- Last name -->
        <div class="form-group">
            <label for="last_name" class="col-md-2 control-label">Last name</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="last_name" name="last_name" value="<?= set_value('last_name', $user->meta->last_name ) ?>" placeholder="<?= lang('form_last_name_placeholder') ?>" />
            </div>
        </div>

        <!-- Password -->
        <div class="form-group">
            <label for="password" class="col-md-2 control-label">New <?= lang('auth.password') ?></label>
            <div class="col-md-3">
                <input type="password" name="password" id="password" class="form-control">
                <?= $uikit->notice(lang('auth.password_strength'), 'default', false, ['class' => 'pass-strength alert-info']); ?>
            </div>
        </div>

        <!-- Confirm Pass -->
        <div class="form-group">
            <label for="pass_confirm" class="col-md-2 control-label"><?= lang('auth.pass_confirm') ?></label>
            <div class="col-md-3">
                <input type="password" name="pass_confirm" id="pass-confirm" class="form-control">
            </div>
        </div>

        <!-- User Group -->
        <div class="form-group">
            <label for="user_group" class="col-md-2 control-label">User Group</label>
            <div class="col-md-3">
                <?= form_dropdown('user_group', $user_group_options, $user->group_id, ['class' => 'form-control']) ?>
            </div>
        </div>

        <!-- Submit -->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-primary" name="submit" value="Update User" />
            </div>
        </div>

    </div>

<?= form_close(); ?>
