<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="<?= site_url('change_password') ?>" class="btn btn-info" role="button">Change Password</a>
        <a href="<?= site_url('admin/users/profile') ?>" class="btn btn-default" role="button">Cancel</a>
    </div>
    <h2 >Update Profile</h2>
</div>

<?= form_open('', array('class' => 'form-horizontal')); ?>

    <div class="row">

        <!-- First name -->
        <div class="form-group">
            <label for="first_name" class="col-md-2 control-label">First name</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="first_name" name="first_name" value="<?= set_value('first_name', $user->meta->first_name) ?>" />
            </div>
        </div>

        <!-- Last name -->
        <div class="form-group">
            <label for="last_name" class="col-md-2 control-label">Last name</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="last_name" name="last_name" value="<?= set_value('last_name', $user->meta->last_name) ?>" />
            </div>
        </div>

        <!-- Email -->
        <div class="form-group">
            <label for="email" class="col-md-2 control-label">Email</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="email" name="email" value="<?= set_value('email', $user->email) ?>" />
            </div>
        </div>

        <?php if ( $user_group === 'agent' ) : ?>

        <!-- Supervisor -->
        <div class="form-group">
            <label for="supervisor" class="col-md-2 control-label">Supervisor</label>
            <div class="col-md-3">
                <?= form_dropdown('supervisor', $supervisor_options, $supervisor, ['class' => 'form-control']) ?>
            </div>
        </div>

        <!-- Snippets -->
        <div class="form-group">
            <label for="snippets" class="col-md-2 control-label">Snippets</label>
            <div class="col-md-3">
                <textarea rows="10" class="form-control" id="snippets" name="snippets"><?= set_value('snippets', $user->meta->snippets) ?></textarea>
            </div>
        </div>

        <?php endif ?>

        <!-- Submit -->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-primary" name="submit" value="Update Profile" />
            </div>
        </div>

    </div>

<?= form_close(); ?>
