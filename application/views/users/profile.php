<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="<?= site_url('change_password') ?>" class="btn btn-info" role="button">Change Password</a>
        <a href="<?= site_url('admin/users/edit/') ?>" class="btn btn-primary" role="button">Edit Profile</a>
    </div>
    <h2 >My Profile <small>Don't flatter yourself...</small></h2>
</div>

<div class="table table-responsive">
    <table class="table table-striped">
        <tbody>
            <tr>
                <th class="col-sm-2">Username</th>
                <td><?= $user->username ?></td>
            </tr>
            <tr>
                <th class="col-sm-2">Email</th>
                <td><?= $user->email ?></td>
            </tr>
            <tr>
                <th class="col-sm-2">First Name</th>
                <td><?= $user->meta->first_name ?></td>
            </tr>
            <tr>
                <th class="col-sm-2">Last Name</th>
                <td><?= $user->meta->last_name ?></td>
            </tr>
            <?php if ( $user_group === 'agent' ) : ?>
            <tr>
                <th class="col-sm-2">Supervisor</th>
                <td><?= empty($user->meta->supervisor) ? '' : get_user_fullname($user->meta->supervisor) ?></td>
            </tr>
            <?php endif ?>
            <tr>
                <th class="col-sm-2">User Group</td>
                <td><?= $user->description ?></td>
            </tr>
        </tbody>
    </table>
</div><!-- /.table-responsive -->
