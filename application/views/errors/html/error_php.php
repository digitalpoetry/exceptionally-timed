<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

?><div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: <?= $severity; ?></p>
<p>Message:  <?= $message; ?></p>
<p>Filename: <?= $filepath; ?></p>
<p>Line Number: <?= $line; ?></p>

<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

    <p>Backtrace:</p>
    <?php foreach (debug_backtrace() as $error): ?>

        <?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

            <p style="margin-left:10px">
            File: <?= $error['file'] ?><br />
            Line: <?= $error['line'] ?><br />
            Function: <?= $error['function'] ?>
            </p>

        <?php endif ?>

    <?php endforeach ?>

<?php endif ?>

</div>