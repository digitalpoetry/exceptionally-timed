<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Mailer
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class TexceptionMailer, {@inheritdoc}.
 */
class TexceptionMailer extends \Myth\Mail\BaseMailer {

    
    /**
     * @var mixed $from Email 'from' field.
     */
    protected $from = null;
    
    /**
     * @var mixed $to Email 'to' field.
     */
    protected $to = null;
    
    /**
     * @var mixed $reply_to Email 'reply_to' field.
     */
    protected $reply_to = null;
    
    /**
     * @var mixed $cc Email 'cc' field.
     */
    protected $cc = null;

    /**
     * Initialize Time Exceptions Mailer
     */
    public function __construct()
    {
        $this->from = 'noreply@codeallthethings.xyz';
    }

    /**
     * Send out a list of approved time exceptions.
     *
     * @param array $texceptions The approved time exceptions.
     * @return bool
     */
    public function processApproved($texceptions)
    {
        $data = [
            'texceptions' => $texceptions,
            'site_name'   => config_item('site.name'),
            'site_link'   => site_url()
        ];

        // Send it immediately - don't queue.
        return $this->send($this->to, 'Approved Time Exceptions, processed on ' . date('Y/m/d g:i a'), $data);
    }

    /**
     * Send out a denied time exception.
     *
     * @param array $texception The denied time exception.
     * @return bool
     */
    public function processDenied($texception)
    {
        $data = [
            'texception' => $texception,
            'site_name'  => config_item('site.name'),
            'site_link'  => site_url()
        ];

        // Send it immediately - don't queue.
        return $this->send($this->to, 'Denied Time Exception, processed on ' . date('Y/m/d g:i a'), $data);
    }

}
