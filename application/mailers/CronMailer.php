<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Mailer
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class CronMailer, {@inheritdoc}.
 */
class CronMailer extends \Myth\Mail\BaseMailer {

    protected $from     = null;
    protected $to       = null;

    public function __construct()
    {
        $this->from = [ config_item('site.auth_email'), config_item('site.name') ];
        $this->to   = config_item('site.auth_email');
    }

    /**
     * Sends the output from the cronjob to the admin.
     *
     * @param $output
     */
    public function results($output=null)
    {
        $data = [
            'output' => $output,
            'site_name' => config_item('site.name')
        ];

        // Send it immediately - don't queue.
        return $this->send($this->to, "Cron Results from ". config_item('site.name'), $data);
    }
}
