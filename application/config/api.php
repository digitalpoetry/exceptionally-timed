<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Authentication Type
 *
 * Which type of authentication to use. Some, like HTTP Basic, still uses the 
 * standard users/passwords of any existing users, providing API access 
 * automatically to all users.
 *
 * Allowed values: `'basic'`, `'digest'`
 */
$config['api.auth_type']    = 'basic';

/**
 * Realm
 *
 * The "realm" that the authentication is protecting. Typically only a single 
 * value per site. Only supported by basic and digest authentication.
 */
$config['api.realm'] = 'Unnamed';

/**
 * Credential Field
 *
 * This is the field in the users table that is considered to be the username of 
 * the client. This will typically be either `'username'` or `'email'`.
 */
$config['api.auth_field']   = 'email';

/**
 * IP Blacklists
 *
 * A comma-separated list of IP addresses that will not be allowed access to the 
 * API under any circumstances.
 */
$config['api.ip_blacklist_enabled']    = false;
$config['api.ip_blacklist']            = '';

/**
 * IP Whitelists
 *
 * A comma-separated list of IP address that are the ONLY IP addresses allowed 
 * to access the site. Any other IP's will be rejected.
 */
$config['api.ip_whitelist_enabled'] = false;
$config['aip.ip_whitelist']         = '';

/**
 * Restrict to SSL Requests
 *
 * If `true`, will only accept API requests that are handled over any *HTTPS* 
 * connection. `false` will allow any request to be handled.
 */
$config['api.require_ssl'] = false;

/**
 * AJAX Requests Only?
 *
 * If `true`, the API will be restricted to only allow calls through *AJAX* 
 * calls. All other traditional calls will be rejected.
 */
$config['api.ajax_only'] = false;

/**
 * Enable API Logging?
 *
 * If enabled, will record performance and monitoring information for this 
 * request.
 */
$config['api.enable_logging'] = false;

/**
 * Enable Rate Limiting?
 *
 * If enabled, will restrict a user to X request per hour. This can be 
 * overridden per controller
 */
$config['api.enable_rate_limits'] = false;

/**
 * Rate Limits
 *
 * The number of requests against the API a user can make per hour. This can be 
 * overridden per controller.
 */
$config['api.rate_limits'] = 100;
