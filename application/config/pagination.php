<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

// Pagination
$config['pagination.config'] = [
    // URI
    'prefix'           => '/page/',
    'use_page_numbers' => true,
    // Wrapper
    'full_tag_open'    => '<ul class="pagination">',
    'full_tag_close'   => '</ul>',
    // Digit
    'num_tag_open'     => '<li>',
    'num_tag_close'    => '</li>',
    // Current
    'cur_tag_open'     => '<li class="disabled"><a href="#">',
    'cur_tag_close'    => '</a></li>',
    // Previous
    'prev_tag_open'    => '<li>',
    'prev_tag_close'   => '</li>',
    'prev_link'        => '<span aria-hidden="true">&laquo;</span>',
    // Next
    'next_tag_open'    => '<li>',
    'next_tag_close'   => '</li>',
    'next_link'        => '<span aria-hidden="true">&raquo;</span>',
    // First
    'first_tag_open'   => '<li>',
    'first_tag_close'  => '</li>',
    'first_link'       => 'First',
    // Last
    'last_tag_open'    => '<li>',
    'last_tag_close'   => '</li>',
    'last_link'        => 'Last',
];