<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * HTML Meta Tag settings
 *
 * This array will be automatically read into the ThemedController's `$meta` 
 * object to set default HTML meta tag values.
 */
$config['meta'] = [
    'x-ua-compatible'   => 'ie=edge',
    'viewport'          => 'width=device-width, initial-scale=1',
];

/**
 * HTTP Equivalent tags.
 *
 * Any tag names listed here will be output with `http-equiv` instead of the 
 * standard `name` attribute.
 */
$config['http-equiv'] = [
    'x-dns-prefetch-control'
];
