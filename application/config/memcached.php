<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Configuration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Memcached settings
 * 
 * Your Memcached servers can be specified below.
 *
 * @see See the user guide for documentation on [Memcached Caching]
 *      (https://codeigniter.com/user_guide/libraries/caching.html#memcached-caching).
 */
$config = array(
    'default' => array(
        'hostname' => '127.0.0.1',
        'port'     => '11211',
        'weight'   => '1',
    ),
);
