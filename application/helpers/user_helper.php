<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Module
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 *
 * @todo Remove unused functions from this helper.
 */

use Myth\Auth\Flat\FlatGroupsModel;

/**
 * Get a user by ID.
 *
 * Returns a user for the given `$user_id`.
 *
 * @uses get_instance() Returns the CodeIgniter super object.
 * 
 * @param  int     $user_id  The ID of the user to be retrieved.
 * @param  str     $select   The user fields that will be retrieved.
 * @param  bool    $withmeta Whether to include the user's metadata.
 * @return array   $user     An array of user data for the given ID.
 */
function get_user_by_id($user_id, $select = 'id', $withmeta = true)
{
    // Get the CI object.
    $ci = get_instance();

    // Check if user meta should be included.
    if ( $withmeta )
    {
        // Get user with meta.
        $user = $ci->user_model->select($select)
                               ->withMeta()
                               ->find($user_id);
    }
    else
    {
        // Get user without meta.
        $user = $ci->user_model->select($select)
                               ->find($user_id);
    }

    return $user;
}

/**
 * Get users by group.
 *
 * Gets all users belonging to the a given group.
 *
 * @uses get_instance() Returns the CodeIgniter super object.
 * 
 * @param  int|str $group
 * @param  str     $select
 * @param  bool    $withmeta Whether to include the user's metadata.
 * @return array   $users
 */
function get_users_by_group($group, $select = 'id', $withmeta = true)
{
    // Get the CI object.
    $ci = get_instance();

    // Get IDs of all users in group supervisor.
    $user_ids = $ci->authorization->getUserIDsByGroup($group);

    // Check if user meta should be included.
    if ( $withmeta )
    {
        // Get users with meta.
        $users = $ci->user_model->select($select)
                                ->withMeta()
                                ->find_many($user_ids);
    }
    else
    {
        // Get users without meta.
        $users = $ci->user_model->select($select)
                                ->find_many($user_ids);
    }

    return $users;
}

/**
 * Get a user's fullname.
 *
 * Returns the fullname of given the user. Catenates the first and last names
 * the user. Accepts a user ID or a user object.
 *
 * @uses get_user_by_id() Gets a user by the user ID.
 * 
 * @param  int|obj $id     The user object to get a fullname for.
 * @return str     $fullname The fullname
 */
function get_user_fullname($id)
{    
    // Return if `$id` is a null value.
    if (is_null($id))
    {
        return;
    }

    // Get the relevant user if a user ID was given.
    if (is_numeric($id))
    {
        $user = get_user_by_id($id);
    }

    // Build the user's fullname.
    $fullname = $user->meta->first_name . ' ' . $user->meta->last_name;

    return $fullname;
}

/**
 * Get the current user's ID.
 *
 * Gets the ID of the currently logged in user.
 * 
 * @uses get_instance() Returns the CodeIgniter super object.
 * 
 * @return int $user_id The current user's ID.
 */
function get_current_user_id()
{
    $ci = get_instance();

    // Get the current user ID
    $user_id = $ci->authenticate->id();

    return $user_id;
}

/**
 * Get the current user.
 *
 * Gets the the currently logged in user.
 *
 * @uses get_user_by_id().
 * 
 * @param  bool  $withmeta Whether to include the user's metadata.
 * @return array $user     An array of the current user's data.
 */
function get_current_user_info($withmeta = true)
{
    // Get the current user ID
    $user_id = get_current_user_id();

    // Get the current user
    $user = get_user_by_id($user_id, '*', $withmeta);

    return $user;
}
