<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Helper
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Get a string representing a boolean value.
 *
 * Returns a string based on the boolean value of `$bool`. Returns `$string[0]` 
 * for **false**, `$string[1]` for **true** and `$string[2]` for **null** or 
 * **empty** values.
 * 
 * @param  Boolean $bool The value to evaluate as boolean.
 * @param  Array   $string {
 *     Optional. Array of strings for **false**,
 *     **true** & **non-boolean** values.
 *     @type String $0 The string for **true** boolean values.
 *     @type String $1 The string for **false** boolean values.
 *     @type String $2 The string for **non-boolean** values. 
 * }
 * @return String  $str  The string for the boolean value of `$bool`.
 */
function bool_to_str($bool, $string = array())
{
    // Set defaults.
    $string = empty($string) ? ['yes', 'no', 'pending'] : $string;

    // Check if `$var` is null.
    if (is_null($bool))
    {
        // Return string for a non-boolean value.
        $str = $string[2];
    }
    else
    {
        // Return string for a true or a false boolean value.
        $str = (int)$bool ? $string[0] : $string[1];
    }

    return $str;
}

/**
 * Format a datetime string.
 *
 * Formats `$datetime` according to the given `$format`. Uses `strtotime()` to 
 * convert `$datetime` into a timestamp before returning the formatted datetime.
 * This means relative time strings may be passed with the `$datetime` parameter.
 *
 * @see http://php.net/strtotime#refsect1-function.strtotime-examples.
 * 
 * @param  str $datetime     The datetime to format.
 * @param  str $format       The format of the new datetime.
 * @return str $new_datetime The formatted datetime.
 */
function datetime_to_str($datetime, $format = 'M j g:i a')
{
    // Return if date is empty or a number/timestamp.
    if (empty($datetime) || is_numeric($datetime))
    {
        return false;
    }

    // Get a timestamp for `$datetime`.
    $timestamp = strtotime($datetime);

    // Get a datetime in given `$format`.
    $new_datetime = date($format, $timestamp);

    return $new_datetime;
}

/**
 * Format a datetime string for an input tag.
 *
 * Formats a string for use with a value attribute of a datetime-local input 
 * HTML tag.
 *
 * @uses datetime_to_str() Formats a datetime string.
 * 
 * @param  str $datetime     The datetime to format.
 * @return str $new_datetime The formatted datetime.
 */
function datetime_to_input($datetime)
{
    // Get the formatted datetime string.
    $datetime = datetime_to_str($datetime, 'Y-m-d H:i');

    // Insert the 'T' into the datetime.
    $new_datetime = str_replace(' ', 'T', $datetime);

    return $new_datetime;
}

/**
 * Format a datetime string with text.
 *
 * Formats `$datetime` with text and the time before the date.
 * 
 * @param  str $datetime     The datetime to format.
 * @return str $new_datetime The formatted datetime.
 */
function datetime_to_text($datetime)
{
    // Get the formatted datetime string.
    $datetime = datetime_to_str($datetime, 'g:i a @ F j, Y');

    // Insert the 'T' into the datetime.
    $new_datetime = str_replace('@', 'on', $datetime);

    return $new_datetime;
}

/**
 * Get 'selected' attribute html.
 *
 * Returns a string for the given `$option` for use as the `selected` attribute
 * of the related HTML tag.
 *
 * @uses set_value() Sets the value of a form field.
 * 
 * @param  str $field   The id of the field this option relates to.
 * @param  str $option  The option value to evaluate for selection.
 * @param  str $default A default value to be selected.
 * @return str $output  An HTML string for the given option.
 */
function is_option_selected($field, $option, $default = '')
{    
    // Get the current set value.
    $selected = set_value($field, $default);
    
    // Set a default value to return.
    $output = '';

    // Check if the set value matches the given option.
    if ($option == $selected)
    {
        $output = ' selected';
    }

    // Return string.
    return $output;
}
