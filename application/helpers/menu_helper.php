<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Helper
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 *
 * @todo Look into moving user related function in this file elsewhere to keep
 *       stuff organized.
 */

use Myth\Navigation\MenuCollection;
use Myth\Navigation\MenuItem;
use Myth\Navigation\Menu;

/**
 * Render html for the menu with the item given.
 *
 * @todo Use a UIKit in this function.
 * @param arr $menu_items The array of menu items to render.
 * @return void Echo html for menu items.
 */
function bootstrap_menu($menu_items)
{
    if ( empty($menu_items) )
    {
        return;
    }

    foreach ($menu_items as $item)
    {
        if ( empty($item->children()) )
        {
            echo '<li><a href="', $item->link(), '">', $item->title(), '</a></li>';
        }
        else
        {
            echo '
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >', $item->title(), '<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        ', bootstrap_menu($item->children()), '
                    </ul>
                </li>', "\n";
        }
    }
}

/**
 * Get menu items.
 * 
 * Returns array of sorted menu items.
 *
 * @uses MenuCollection::menu()
 * @uses populate_menu()
 * @uses populate_menu()
 * 
 * @param obj $menu     The name of the menu.
 * @param str $location The location of the menu in the theme.
 * @return arr A multi-dementional array of menu items.
 */
function get_menu_items($menu, $location)
{
    // Get menu from config
    $menu_items = config_item('menu.' . $location . '.' . $menu);

    // Populate the menu
    populate_menu($menu_items, $location);

    // Get menu instance
    $menu = MenuCollection::menu($location);

    // Return sorted menu items
    return $menu->sortBy('order')->items();
}

/**
 * Reursively adds items and submenus to a menu.
 * 
 * @uses MenuCollection::menu()
 * 
 * @param arr $items    The item to be added to the menu.
 * @param str $location The location of the menu in the theme.
 * @param str $parent   Optional. The parent name if `$items` are a submenu.
 * @return void
 */
function populate_menu($items, $location, $parent = '')
{
    // Get menu instance
    $menu = MenuCollection::menu($location);

    foreach ($items as $item)
    {
        extract($item, EXTR_OVERWRITE);
        $link  = ($relative)   ? site_url($link) : $link;
        $link  = empty($link)  ? '#' : $link;
        $glyph = empty($glyph) ? ''  : $glyph;
        $order = empty($order) ? 100 : $order;

        // Create menu item
        $menu_item = new MenuItem($name, $title, $link, $glyph, $order);

        // Add item to menu
        if ( empty($parent) )
        {
            $menu->addItem($menu_item);
        }
        else
        {
            $menu->addChild( $menu_item, $parent);                
        }

        // Add item submenu
        if ( ! empty($submenu) )
        {
            populate_menu($submenu, $location, $name);
        }
    }
}
