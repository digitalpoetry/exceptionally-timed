# UNSORTED NOTES

- [GIT Keyword expansion](http://forum.diyefi.org/viewtopic.php?f=45&t=2117).
- [Version numbering](http://thomas-cokelaer.info/blog/2014/12/moving-from-svn-to-git-id-equivalent/) with GIT. 
- [See Also](http://stackoverflow.com/questions/16524225/how-can-i-populate-the-git-commit-id-into-a-file-when-i-commit#answer-16524375)
- [here too](https://wincent.com/blog/automatic-deployment-numbering-in-git)
- [lastly](https://www.quora.com/How-do-I-include-Git-commit-hash-in-managed-source-code).
- [PHP Annotations ](http://php-annotations.readthedocs.io/en/latest/ConsumingAnnotations.html)
- [Faker API Reference](http://apigen.juzna.cz/doc/fzaninotto/Faker/source-class-Faker.Provider.Internet.html#59-65)
- [Faker Base Functions](http://apigen.juzna.cz/doc/fzaninotto/Faker/class-Faker.Provider.Base.html#___construct)
- [Kint Debugging](https://github.com/adriangonzales/codeigniter-kint)

- [Code Folding, SublimeText](https://packagecontrol.io/packages/SyntaxFold)
  - [Readme](https://github.com/jamalsenouci/sublimetext-syntaxfold#readme)
- [WOWOW!](http://www.grocerycrud.com/)