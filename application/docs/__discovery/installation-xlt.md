# Install XLT
Based on the [Simple Blog](http://catt.dev/docs/developer/tutorials/blog) tutorial.
Must complete catt-installation.md first!


## Migrations
Generates a table migration [Migrations Reference](http://catt.dev/docs/developer/forge/generators#migration).

```bash
    cd xlt
php sprint forge migration create_texceptions_table -fields "\
    id:id \
    supervisor:number \
    start:datetime \
    end:datetime"
    authorized_by:number \
    reason:string \
    approved:tinyint:1 \
    approved_by:number \
    approved_on:datetime \
    created_on:datetime \
    modified_on:datetime \
    deleted:tinyint:1 \
    deleted_by:number"
```


Modify `application/database/migrations/XXXX_Create_texceptions_table.php`.

Create Database Table:
```bash
php sprint database migrate
```


## Scaffolds
Generates:
- `application/controllers/Texceptions.php` - A controller that handles the basic CRUD operations for us, [Admin Controller Reference](http://catt.dev/docs/developer/tutorials/blog#admin_controller).
- `application/models/Texception_model.php` - Our base model based on CIDbModel that provides many useful methods to work with our database table, [Form Validation Reference](http://www.codeigniter.com/userguide3/libraries/form_validation.html#rule-reference).
- `application/database/seeds/TexceptionsSeeder.php` - An empty seed file we will use to enter generic test data, [Seeding Reference](http://catt.dev/index.php/docs/developer/database/seeding).
- `application/views/texceptions/index.php` - View to list all time exceptions
- `application/views/texceptions/create.php` - View with the creation form for new time exceptions
- `application/views/texceptions/show.php` - View to show a single form
- `application/views/texceptions/update.php` - View with the editing form for existing time exceptions

```bash
php sprint forge scaffold texceptions
```


## Roles and Capabilities
Modify:
- `application/database/seeds/TexceptionsAuthorizationSeeder.php`.
- `application/database/seeds/TexceptionsSeeder.php`.

Seed Database Tables:
```bash
php sprint database seed TexceptionsAuthorizationSeeder
php sprint database seed TexceptionsSeeder
```


## Route to Admin Area
Modify:
- `application/controllers/Texceptions.php`, [Admin Controller Reference](http://catt.dev/docs/developer/tutorials/blog#admin_controller).
- `application/config/routes.php`, [Admin Area Reference](http://catt.dev/docs/developer/tutorials/blog#admin_area).


## The _ View
Modify `/application/views/texceptions/*.php`, [View Reference](http://catt.dev/docs/developer/tutorials/blog#the_view).


## Site Structure
    > catt.dev            /application/views/home/index.php
    >         /admin/texceptions            /texceptions/index.php
    >                           /manage                 /manage.php
    >                           /create                 /create.php
    >                           /view                   /view.php
    >                           /update                 /update.php
    >         /admin/review                 /review/index.php
    >                      /team                   /team.php
    >                      /agent                  /agent.php
    >         /admin/reports                /reports/index.php
    >                       /team                   /team.php
    >                       /agent                  /agent.php
    >         /admin/users                  /user/index.php
    >                     /profile               /profile.php
    >                     /settings              /settings.php
