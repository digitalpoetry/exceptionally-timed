
DocBlock Templates
====================

TODO:
- [ ] Review @version tags
- [ ] Add annotations to DocBlock Templates

****************************************

#### Markers

Mark code for improvements:
```php
// TODO: [description]
```

Mark code for bugs:
```php
// FIXME: [description]
```

****************************************

#### Single Line Comments

Not a DocBlock:
```php
// Allow plugins to filter an array.
```

DocBlock:
```php
/** Allow plugins to filter an array. */
```

****************************************   

#### Multi-line Comments 

Not a DocBlock:
```php
/* 
 * This is a comment that is long enough to warrant being stretched over
 * the span of multiple lines. You'll notice this follows basically
 * the same format as the PHPDoc wrapping and comment block style.
 */
```

DocBlock:
```php
/**
 * This is a comment that is long enough to warrant being stretched over
 * the span of multiple lines. You'll notice this follows basically
 * the same format as the PHPDoc wrapping and comment block style.
 */
```

****************************************

#### File

Standard Usage:
```php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     CATT
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource	
 */
```

Complete Usage:
```php
/**
 * Summary (no period for file headers)
 *
 * Description.
 *
 * @package     CATT
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @version		0.1.0 Shiny Things
 * @since		0.0.0 Basic Things.
 * @link        http://codeallthethings.xyz Code-All-The-Things
 * @filesource
 * 
 * @todo        Description.
 */
```

****************************************

#### Require(_once), Include(_once)

Standard Usage:
```php
/**
 * Summary.
 *
 * @since	0.0.0 Shiny Things
 * @see     [URI | FQSEN] [<description>]
 * @link    http://codeallthethings.xyz Code All The Things.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * @since	0.0.0 Shiny Things
 * @see     [URI | FQSEN] [<description>]
 * @link	http://codeallthethings.xyz Code All The Things.
 * @todo	Description.
 */
```

****************************************

#### Class

Standard Usage:
```php
/**
 * Summary.
 *
 * Description, {@inheritdoc}.
 *
 * @inheritdoc
 * @since	0.0.0 Basic Things.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * Description, {@inheritdoc}.
 *
 * @inheritdoc
 * @author		Jesse LaReaux <jlareaux@gmail.com>
 * @copyright	Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license		http://opensource.org/licenses/MIT MIT License
 *
 * @package		CATT
 * @version     0.1.0 Shiny Things
 * @since		0.0.0 Basic Things
 * @deprecated	x.x.x Use new_function_name().
 * @see			new_function_name().`
 *
 * @uses		fqsen <description>
 * @see			url|fqsen <description>
 * @example		location <start> <length> <description>
 * @link		http://codeallthethings.xyz Code All The Things.
 * @source		<start> <length> <description>
 * @todo		Description.
 */
```

****************************************

#### Function

Standard Usage:
```php
/**
 * Summary.
 *
 * Description.
 *
 * @version	0.1.0 Shiny Things
 * @since   0.0.0 Basic Things
 *
 * @global	type $var <description>
 * @uses	fqsen <description>
 * @see		url|fqsen <description>
 * @link	http://codeallthethings.xyz Code All The Things.
 *
 * @param	type $var Description.
 * @return	type Description.
 * @throws	type Description.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * Description.
 *
 * @version		<vector> <description>
 * @since       0.0.0 Basic Things
 * @deprecated	x.x.x Use new_function_name().
 * @see			new_function_name().
 *
 * @global		type $var <description>
 * @uses		fqsen <description>
 * @see			url|fqsen <description>
 * @example		location <start> <length> <description>
 * @link		http://codeallthethings.xyz Code All The Things.
 * @source		<start> <length>
 * @todo		Description.
 *
 * @param	type $var Description.
 * @return	type Description.
 * @throws	type Description.
 */
```

****************************************

#### Method

Standard Usage:
```php
/**
 * Summary.
 *
 * Description, {@inheritdoc}.
 *
 * @inheritdoc
 * @api
 * @since   0.0.0 Basic Things
 *
 * @global	type $var <description>
 * @uses	fqsen <description>
 * @see		url|fqsen <description>
 * @link	http://codeallthethings.xyz Code All The Things.
 *
 * @param	type $var Description.
 * @return	type Description.
 * @throws	type Description.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * Description, {@inheritdoc}.
 *
 * @inheritdoc
 * @api
 * @since       0.0.0 Basic Things
 * @deprecated	x.x.x Use new_method_name().
 * @see			new_method_name().
 *
 * @global		type $var <description>
 * @uses		fqsen <description>
 * @see			url|fqsen <description>
 * @example		location <start> <length> <description>
 * @link		http://codeallthethings.xyz Code All The Things.
 * @source		<start> <length>
 * @todo		Description.
 *
 * @param	type $var Description.
 * @return	type Description.
 * @throws	type Description.
 */
```

****************************************

#### Property

Standard Usage:
```php
/**
 * Summary, {@inheritdoc}.
 *
 * @inheritdoc
 * @version	0.1.0 Shiny Things
 * @since   0.0.0 Basic Things
 * @var		type $var Description.
 */
```

Complete Usage:
```php
/**
 * Summary, {@inheritdoc}.
 *
 * @inheritdoc
 * @version		0.1.0 Shiny Things
 * @since       0.0.0 Basic Things
 * @deprecated	x.x.x Use new_property_name().
 * @see			new_property_name().
 *
 * @see			url|fqsen <description>
 * @example		location <start> <length> <description>
 * @link		http://codeallthethings.xyz Code All The Things.
 * @source		<start> <length> <description>
 * @todo		Description.
 *
 * @var			type $var Description.
 */
```

****************************************

#### [class] Constant:

Standard Usage:
```php
/**
 * Summary.
 *
 * @version	0.1.0 Shiny Things
 * @since   0.0.0 Basic Things
 * @var		type $var Description.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * @inheritdoc
 * @version		0.1.0 Shiny Things
 * @since       0.0.0 Basic Things
 * @deprecated	x.x.x Use new_class_name().
 * @see			new_property_name().
 *
 * @see			url|fqsen <description>
 * @example		location <start> <length> <description>
 * @link		http://codeallthethings.xyz Code All The Things.
 * @source		<start> <length> <description>
 * @todo		Description.
 *
 * @var			type $var Description.
 */
```

****************************************

#### Global Variable

Standard Usage:
```php
/**
 * Summary.
 *
 * @version	0.1.0 Shiny Things
 * @since   0.0.0 Basic Things
 * @global	type Description.
 */
```

Complete Usage:
```php
/**
 * Summary.
 *
 * @version		0.1.0 Shiny Things
 * @since       0.0.0 Basic Things
 * @deprecated	x.x.x Use new_global_name().
 * @see			new_property_name().
 *
 * @global 		type Description.
 * @see			url|fqsen <description>
 * @example		location <start> <length> <description>
 * @link		http://codeallthethings.xyz Code All The Things.
 * @source		<start> <length> <description>
 * @todo		Description.
 */
```

****************************************

#### Param, Type, Property, Etc

```php
/**
 * @param string		$var	Optional. The _ string.  Default is _.
 * @param integer		$var	Optional. The number of _. Default is _.
 * @param float			$var	Optional. The value of _. Default is _.
 * @param number		$var	Optional. The _ number. Default is _.
 * @param boolean		$var	Optional. _ if set to true, _ if false. Default is _.
 * @param array			$var	Optional. An array of _. Default is _.
 * @param object		$var	Optional. The _ object. Default is _.
 * @param static		$var	Optional. The _ instance. Default is _.
 * @param mixed			$var	Optional. The value of _. Default is _.
 * @param null			$var	Optional. An empty value. Default is _.
 * @param resource		$var	Optional. The _ file handler. Default is _.
 * @param callable		$var	Optional. The _ handler. Default is _.
 * @param (type|type)	$var	Optional. Description. Default is _.
 * @param string[]		$var	Optional. An array of _ strings. Default is _.
 * @param array[]		$var	Optional. An array of _ arrays. Default is _.
 * @param object[]		$var	Optional. An array of _ objects. Default is _.
 * @param mixed[]		$var	Optional. An array of _ values. Default is _.
 * @param array		$args {
 *		Optional. An array of _. Default is _
 *
 *		@type type		$key 	Optional. Accepts 'value', 'value'. (align with
 *								description if wraps to a new line) Default is _.
 * 		@type string	$key	Optional. The _ string. Default is _.
 * 		@type integer	$key	Optional. he number of _. Default is _.
 * 		@type object	$key	Optional. The _ object. Default is _.
 * 		@type boolean	$key	Optional. _ if set to true, _ if false. Default is _.
 * }
 */
```

****************************************

References:
- [phpDoc Documentation](https://www.phpdoc.org/docs/latest/index.html)
- [PSR-5: PHPDoc Standard](https://github.com/phpDocumentor/fig-standards/tree/master/proposed)
- [PSR-?: PHPDoc Standard Revision](https://github.com/mvriel/phpDocumentor2/blob/b803b549189926ae6880cabd699675f22d8aade1/docs/PSR.md)
- [PhpDoc2Cheatsheet](http://phpdoc2cheatsheet.com/)
- [WordPress PHP Documentation Standards](https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/php/#resources)
