# Todo
- Make function to replace `defined('BASEPATH') OR exit('No direct script access allowed');` ?
- Replace TRUE|FALSE with true|false.
- Use HTML helpers in view|theme files.
- Chage form selects to an input w/ a datalist, [see](http://stackoverflow.com/questions/25616625/how-to-display-the-text-in-datalist-html5-and-not-value).
- Refactor $item->supervisor to $item->supervisor_id
- What if an agent doesnt have a their default supervisor saved? `$item->supervisor = get_user_fullname($supervisor);`
- Make sure users are a soft delete so `get_user_fullname()` won't throw errors trying to format deleted users.
- I10n all strings in the whole damn application.