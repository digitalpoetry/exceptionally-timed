# Install CATT
The minimum setup required for CATT. The `base_url` must end with `.dev` or 
the development configuration will not be loaded.


### Install with Composer
See [Reference](https://github.com/ci-bonfire/Sprint/blob/develop/myth/_docs_src/installation.md) installation.


Automatic install:
```bash
php composer.phar create-project sprintphp/sprintphp xlt dev-develop
```


Manual install:
```bash
composer install
composer dump-autoload
php build/build.php postCreateProject
composer dump-autoload -o
```


Modify `/application/config/application.php`:
- `$config['site.name']        = 'Exceptionally Timed';`
- `$config['site.auth_email']  = 'jlareaux@godaddy.com'`


Migrate the Database:
```bash
php sprint database migrate
```


Seed Database Tables:
```bash
php sprint database seed FlatAuthorizationSeeder
```


Login:
Signup and then login to Sprint.
