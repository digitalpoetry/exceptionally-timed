<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Seeder
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */


/**
 * Class FlatAuthorizationSeeder
 *
 * Inserts sample groups and permissions to work with.
 */
class FlatAuthorizationSeeder extends Seeder {

    /**
     * Run the seeder.
     *
     * @return void
     */
    public function run()
    {
        $flat = new \Myth\Auth\FlatAuthorization();

        // User Groups
        $flat->createGroup('administrator', 'Administrators');
        #$flat->createGroup('member', 'Members');

        // Permission Types
        $flat->createPermission('viewUser', 'View your profile.');
        $flat->createPermission('manageUser', 'Edit your profile.');
        $flat->createPermission('viewOtherUsers', 'View other users.');
        $flat->createPermission('manageOtherUsers', 'Manage other users.');

        // Admin Privileges
        $flat->addPermissionToGroup('viewUser', 'administrator');
        $flat->addPermissionToGroup('manageUser', 'administrator');
        $flat->addPermissionToGroup('viewOtherUsers', 'administrator');
        $flat->addPermissionToGroup('manageOtherUsers', 'administrator');
        // Member Privileges
        #$flat->addPermissionToGroup('viewUser', 'member');
        #$flat->addPermissionToGroup('manageUser', 'member');
    }
    
}
