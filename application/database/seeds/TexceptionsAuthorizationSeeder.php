<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Seeder
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */


/**
 * Class FlatAuthorizationSeeder
 *
 * Inserts sample groups and permissions to work with.
 */
class TexceptionsAuthorizationSeeder extends Seeder {

    /**
     * Run the seeder.
     *
     * @return void
     */
    public function run()
    {
        $flat = new \Myth\Auth\FlatAuthorization();

        // User Groups
        $flat->createGroup('agent', 'Agents');
        $flat->createGroup('supervisor', 'Supervisors');

        // Permission Types
        $flat->createPermission('createTexception', 'Create a time exception.');
        $flat->createPermission('viewTexception', 'View a time exception.');
        $flat->createPermission('updateTexception', 'Update a time exception.');
        $flat->createPermission('deleteTexception', 'Delete a time exception.');
        $flat->createPermission('manageTexceptions', 'Manage time exceptions.');
        $flat->createPermission('createOthersTexception', 'Create other\'s time exceptions.');
        $flat->createPermission('viewOthersTexception', 'View other\'s time exceptions.');
        $flat->createPermission('updateOthersTexception', 'Update other\'s time exceptions.');
        $flat->createPermission('deleteOthersTexception', 'Delete other\'s time exceptions.');
        $flat->createPermission('manageOthersTexceptions', 'Manage other\'s time exceptions.');

        // Admin Privileges
        $flat->addPermissionToGroup('createTexception', 'admin');
        $flat->addPermissionToGroup('viewTexception', 'admin');
        $flat->addPermissionToGroup('updateTexception', 'admin');
        $flat->addPermissionToGroup('deleteTexception', 'admin');
        $flat->addPermissionToGroup('manageTexceptions', 'admin');
        $flat->addPermissionToGroup('createOthersTexception', 'admin');
        $flat->addPermissionToGroup('viewOthersTexception', 'admin');
        $flat->addPermissionToGroup('updateOthersTexception', 'admin');
        $flat->addPermissionToGroup('deleteOthersTexception', 'admin');
        $flat->addPermissionToGroup('manageOthersTexceptions', 'admin');
        // Supervisor Privileges
        $flat->addPermissionToGroup('createTexception', 'supervisor');
        $flat->addPermissionToGroup('viewTexception', 'supervisor');
        $flat->addPermissionToGroup('updateTexception', 'supervisor');
        $flat->addPermissionToGroup('deleteTexception', 'supervisor');
        $flat->addPermissionToGroup('manageTexceptions', 'supervisor');
        $flat->addPermissionToGroup('createOthersTexception', 'supervisor');
        $flat->addPermissionToGroup('viewOthersTexception', 'supervisor');
        $flat->addPermissionToGroup('updateOthersTexception', 'supervisor');
        $flat->addPermissionToGroup('deleteOthersTexception', 'supervisor');
        $flat->addPermissionToGroup('manageOthersTexceptions', 'supervisor');
        // Agent Privileges
        $flat->addPermissionToGroup('createTexception', 'agent');
        $flat->addPermissionToGroup('viewTexception', 'agent');
        $flat->addPermissionToGroup('updateTexception', 'agent');
        $flat->addPermissionToGroup('deleteTexception', 'agent');
        $flat->addPermissionToGroup('manageTexceptions', 'agent');
    }
    
}
