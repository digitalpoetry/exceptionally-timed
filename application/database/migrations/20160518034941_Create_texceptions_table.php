<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Migration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Migration: Create Texceptions Table
 *
 * Created by: CATT
 * Created on: 2016-05-18 01:51am
 *
 * @property $dbforge
 */
class Migration_create_texceptions_table extends CI_Migration {

    /**
     * Imports the migration
     *
     * @return void
     */
    public function up ()
    {

    	// Define the table structure
        $fields = array(
            'id'                 => array(
                'type'           => 'int',
                'unsigned'       => true,
                'auto_increment' => true,
                'constraint'     => 9,
            ),
            'created_by'         => array(
                'type'           => 'int',
                'constraint'     => 9,
                'null'           => false,
            ),
            'supervisor'         => array(
                'type'           => 'int',
                'constraint'     => 9,
                'null'           => false,
            ),
            'start'              => array(
                'type'           => 'datetime',
                'null'           => false,
            ),
            'end'                => array(
                'type'           => 'datetime',
                'null'           => false,
            ),
            'authorized_by'      => array(
                'type'           => 'int',
                'constraint'     => 9,
                'null'           => true,
            ),
            'reason'             => array(
                'type'           => 'varchar',
                'constraint'     => 255,
                'null'           => false,
            ),
            'approved'           => array(
                'type'           => 'tinyint',
                'constraint'     => 1,
                'null'           => true,
            ),
            'approved_by'        => array(
                'type'           => 'int',
                'constraint'     => 9,
                'null'           => true,
            ),
            'approved_on'        => array(
                'type'           => 'datetime',
                'null'           => true,
            ),
            'created_on'         => array(
                'type'           => 'datetime',
                'null'           => false,
            ),
            'modified_on'        => array(
                'type'           => 'datetime',
                'null'           => true,
            ),
            'deleted'            => array(
                'type'           => 'tinyint',
                'constraint'     => 1,
                'default'        => 0,
            ),
            'deleted_by'         => array(
                'type'           => 'int',
                'constraint'     => 9,
                'null'           => true,
            )
        );

        // Add the fields, set a primary key & create the table
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
	    $this->dbforge->create_table('texceptions', true, config_item('migration_create_table_attr') );
    
    }

    /**
     * Removes the migration
     *
     * @return void
     */
    public function down ()
    {
        $this->dbforge->drop_table('texceptions');
    }

}
