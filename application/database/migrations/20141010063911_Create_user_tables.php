<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Module
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Migration: Create User Tables
 *
 * Created by: CATT
 * Created on: 2014-10-10 06:39:11 am
 *
 * @property $dbforge
 */
class Migration_create_user_tables extends CI_Migration {

    /**
     * Import the migration
     *
     * @return void
     */
    public function up ()
    {
        // Users
        $fields = [
            'id'               => [
                'type'           => 'int',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true
            ],
            'email'            => [
                'type'           => 'varchar',
                'constraint'     => 255
            ],
            'username'         => [
                'type'           => 'varchar',
                'constraint'     => 30,
                'null'           => true
            ],
            'password_hash'    => [
                'type'           => 'varchar',
                'constraint'     => 255
            ],
            'reset_hash'       => [
                'type'           => 'varchar',
                'constraint'     => 40,
                'null'           => true
            ],
            'activate_hash'    => [
                'type'           => 'varchar',
                'constraint'     => 40,
                'null'           => true
            ],
            'status'           => [
                'type'           => 'varchar',
                'constraint'     => 255,
                'null'           => true
            ],
            'status_message'   => [
                'type'           => 'varchar',
                'constraint'     => 255,
                'null'           => true
            ],
            'active'           => [
                'type'           => 'tinyint',
                'constraint'     => 1,
                'null'           => false,
                'default'        => 0
            ],
            'force_pass_reset' => [
                'type'           => 'tinyint',
                'constraint'     => 1,
                'null'           => false,
                'default'        => 0
            ],
            'created_on'       => [
                'type'           => 'datetime',
                'default'        => '0000-00-00 00:00:00'
            ],
            /*
            'created_by'       => [
                'type'           => 'int',
                'constraint'     => 11,
                'unsigned'       => true,
                'default'        => null
            ],
            'modified_on'      => [
                'type'           => 'datetime',
                'default'        => '0000-00-00 00:00:00'
            ],
            'modified_by'      => [
                'type'           => 'int',
                'constraint'     => 11,
                'unsigned'       => true,
                'default'        => null
            ],
            */
            'deleted'          => [
                'type'           => 'tinyint',
                'constraint'     => 1,
                'null'           => 0,
                'default'        => 0
            ],
            /*
            'deleted_by'      => [
                'type'           => 'int',
                'constraint'     => 11,
                'unsigned'       => true,
                'default'        => null
            ],
            */
        ];

        // Set table fields and keys
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_key('email');

        // Create the table
        $this->dbforge->create_table('users', true, config_item('migration_create_table_attr'));

        // User Meta
        $fields = [
            'user_id'    => [
                'type'       => 'int',
                'constraint' => 11,
                'unsigned'   => true
            ],
            'meta_key'   => [
                'type'       => 'varchar',
                'constraint' => 255
            ],
            'meta_value' => [
                'type'       => 'text',
                'null'       => true
            ],
        ];

        // Set table fields and keys
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key(['user_id', 'meta_key'], true);

        // Create the table
        $this->dbforge->create_table('user_meta', true, config_item('migration_create_table_attr'));

    }

    /**
     * Remove the migration
     *
     * @return void
     */
    public function down ()
    {
        $this->dbforge->drop_table('users');
        $this->dbforge->drop_table('user_meta');
    }
}
