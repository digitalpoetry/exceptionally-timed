<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Module
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

#use Myth\Auth\Flat\FlatGroupsModel;
use \Myth\Route;

/**
 * Users Controller
 */
class Users extends AdminController {

    /**
     * @var string $model_file If set, this model file will automatically be 
     *                       loaded.
     */
    protected $model_file = 'user_model';

    /**
     * Initialize the User Controller.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Load files
        $this->lang->load('users');
    }

    /**
     * The default method called. Typically displays an overview of this
     * controller's domain.
     * 
     * @return void
     */
    public function index()
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Load files
        #$this->load->library('table');
        $this->load->library('pagination');
        $this->load->config('pagination');

        // Get the requested users
        $segment = $this->uri->segment( $this->uri->total_segments() );
        $offset  = $this->limit * (intval($segment) > 1 ? $segment - 1 : 0);
        $users   = $this->user_model
                       ->select('users.*, auth_groups.description')
                       ->join('auth_groups_users', 'auth_groups_users.user_id = users.id', 'left')
                       ->join('auth_groups', 'auth_groups_users.group_id = auth_groups.id', 'left')
                       ->withMeta()
                       ->limit($this->limit, $offset)
                       ->find_all();

        // Pagination
        $pagination_config                = config_item('pagination.config');
        $pagination_config['base_url']    = site_url('admin/users');
        $pagination_config['total_rows']  = $this->user_model->count_all();
        $pagination_config['uri_segment'] = $this->uri->total_segments();
        $pagination_config['per_page']    = $this->limit; 
        $this->pagination->initialize($pagination_config);

        // Make vars available in views
        $this->setVar('users', $users);
        $this->setVar('pagination', $this->pagination->create_links());

        $this->render();
    }

    /**
     * Create a single user.
     *
     * @return void
     */
    public function create()
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Load files
        $this->load->helper('form');

        // Check for a $_POST.
        if ($this->input->post())
        {
            // Get the submitted data
            $post_data = $this->input->post();

            // Set additional data
            $post_data['active'] = 1;

            // Create the user
            $id = $this->user_model->insert($post_data);
            if ( $id && $this->authorization->addUserToGroup($id, $post_data['user_group']) )
            {
                $this->setMessage(lang('auth.did_register'), 'success');
                redirect( site_url('admin/users') );
            }
            else
            {
                $this->setMessage($this->user_model->error(), 'danger');
            }
        }

        // Add custom scripts
        $this->addScript('register.js');

        // Get data for views
        foreach ($this->authorization->getAllGroups() as $group)
        {
            $user_group_options[$group['id']] = $group['description'];
        }

        // Set vars for views
        $this->setVar('user_group_options', $user_group_options);

        $this->render();
    }

    /**
     * Displays a single user.
     *
     * @param  int $id  The user ID.
     * @return void
     */
    public function show($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Get the requested $user
        $user = $this->user_model
                     ->select('users.*, auth_groups.description')
                     ->join('auth_groups_users', 'auth_groups_users.user_id = users.id', 'left')
                     ->join('auth_groups', 'auth_groups_users.group_id = auth_groups.id', 'left')
                     ->withMeta()
                     ->find_by("users.id = {$id}");

        // Check $user for an empty result
        if (! $user)
        {
            $this->setMessage('Unable to find that user.', 'warning');
            redirect( site_url('admin/users') );
        }

        // Make vars available to views
        $this->setVar('user', $user);
        if ( $this->authorize->inGroup('agent', $id) )
        {
            $this->setVar('user_group', 'agent');
        }
        elseif ( $this->authorize->inGroup('supervisor', $id) )
        {
            $this->setVar('user_group', 'supervisor');
        }

		$this->render();
    }

    /**
     * Displays current user profile.
     *
     * @return void
     */
    public function profile()
    {
        // Get the requested $user
        $id = $this->authenticate->id();

        // Get the requested $user
        $user = $this->user_model
                     ->select('users.*, auth_groups.description')
                     ->join('auth_groups_users', 'auth_groups_users.user_id = users.id', 'left')
                     ->join('auth_groups', 'auth_groups_users.group_id = auth_groups.id', 'left')
                     ->withMeta()
                     ->find_by("users.id = {$id}");

        // Check $user for an empty result
        if (! $user)
        {
            $this->setMessage('Unable to find that user.', 'warning');
            redirect( site_url('admin') );
        }

        // Make vars available to views
        $this->setVar('user', $user);
        if ( $this->authorize->inGroup('agent', $id) )
        {
            $this->setVar('user_group', 'agent');
        }
        elseif ( $this->authorize->inGroup('supervisor', $id) )
        {
            $this->setVar('user_group', 'supervisor');
        }

        $this->render();
    }

    /**
     * Updates a single user.
     *
     * @param  int $id  The user ID.
     * @return void
     */
    public function update($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Load libraries and helpers.
        $this->load->helper('form');
        $this->load->helper('inflector');

        // Check for a $_POST.
        if ($this->input->method() == 'post')
        {
            // Get the submitted data
            $post_data = $this->input->post();

            // Update the user
            if ( $this->user_model->update($id, $post_data) &&
                 $this->authorization->removeUserFromGroups($id) &&
                 $this->authorization->addUserToGroup($id, $post_data['user_group']) )
            {
                $this->setMessage('Successfully updated user.', 'success');
                redirect( site_url('admin/users/show/' . $id) );
            }

            $this->setMessage('Error updating user. '. $this->user_model->error(), 'danger');
        }

        // Add custom scripts
        $this->addScript('register.js');

        // Get the requested $user.
        $user = $this->user_model
                     ->select('users.*, auth_groups_users.group_id')
                     ->join('auth_groups_users', 'auth_groups_users.user_id = users.id', 'left')
                     ->withMeta()
                     ->find_by("users.id = {$id}");

        // Get data for views
        foreach ($this->authorization->getAllGroups() as $group)
        {
            $user_group_options[$group['id']] = $group['description'];
        }

        // Make $user available in the view.
        $this->setVar('user',               $user);
        $this->setVar('user_group_options', $user_group_options);

        $this->render();
    }

    /**
     * Updates current user profile.
     *
     * @uses get_users_by_group() Gets all users belonging to the a given group.
     *
     * @return void
     */
    public function edit()
    {
        // Load files
        $this->load->helper('form');
        $this->load->helper('inflector');

        // Get the user ID
        $id = $this->authenticate->id();

        // Check for a $_POST.
        if ($this->input->method() == 'post')
        {
            // Get the submitted data
            $post_data = $this->input->post();

            // Update the user
            if ($this->user_model->update($id, $post_data))
            {
                $this->setMessage('Successfully updated user.', 'success');
            }
            else
            {
                $this->setMessage('Error updating user. '. $this->user_model->error(), 'danger');
            }
        }

        // Get the requested $user.
        $user = $this->user_model->withMeta()->find($id);

        // Get data for views
        $supervisor = empty($user->meta->supervisor) ? '' : $user->meta->supervisor;
        foreach (get_users_by_group('supervisor') as $option)
        {
            $supervisor_options[$option->id] = get_user_fullname($option->id);
        }

        // Make vars available to views
        $this->setVar('user',               $user);
        $this->setVar('supervisor',         $supervisor);
        $this->setVar('supervisor_options', $supervisor_options);
        if ( $this->authorize->inGroup('agent', $id) )
        {
            $this->setVar('user_group', 'agent');
        }
        elseif ( $this->authorize->inGroup('supervisor', $id) )
        {
            $this->setVar('user_group', 'supervisor');
        }

        $this->render();
    }

    /**
     * Deletes a single user
     *
     * @param  int $id  The user ID.
     * @return void
     */
    public function delete($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        if ($this->user_model->delete($id))
        {
            $this->setMessage('Successfully deleted user.', 'success');
            redirect( site_url('admin/users') );
        }

        $this->setMessage('Error deleting user. '. $this->user_model->error(), 'danger');
        redirect( site_url( 'admin/users' ) );
    }

    /**
     * Force a password reset for a user
     *
     * @param  int $id  The user ID.
     * @return void
     */
    public function force_reset($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        if ( $this->user_model->update($id, ['force_pass_reset' => 1], true) )
        {
            $this->setMessage('Successfully updated user.', 'success');
            redirect( site_url('admin/users/') );
        }

        $this->setMessage('Error updating user. '. $this->user_model->error(), 'danger');
        redirect( site_url( 'admin/users' ) );
    }

}
