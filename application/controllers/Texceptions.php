
<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Controller
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

use Myth\Auth\Flat\FlatGroupsModel;
use Myth\Mail\Mail;

/**
 * Texceptions Controller
 */
class Texceptions extends AdminController {

    /**
     * @var string $model_file If set, this model file will automatically be 
     *                         loaded.
     */
    protected $model_file = 'texception_model';

    /**
     * Initialize the Texception Controller.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Load files
        $this->load->helper('user');

        // Time Exception styles
        $this->addStyle('texceptions.css');
    }

    /**
     * The default method called. Typically displays an overview of this
     * controller's domain.
     * 
     * @return void
     */
    public function index()
    {
        // Load files
        $this->load->library('pagination');
        $this->load->config('pagination');

        // Get the requested items
        $segment      = $this->uri->segment( $this->uri->total_segments() );
        $offset       = $this->limit * (intval($segment) > 1 ? $segment - 1 : 0);
        $texceptions  = $this->texception_model
                             ->where('created_by', $this->authenticate->id())
                             ->limit($this->limit, $offset)
                             ->find_all();
        // Pagination
        $pagination_config                = config_item('pagination.config');
        $pagination_config['base_url']    = site_url('admin/texceptions');
        $pagination_config['total_rows']  = $this->texception_model->count_by('created_by', $this->authenticate->id());
        $pagination_config['uri_segment'] = $this->uri->total_segments();
        $pagination_config['per_page']    = $this->limit; 
        $this->pagination->initialize($pagination_config);

        // Make vars available in views
        $this->setVar('texceptions', $texceptions);
        $this->setVar('pagination', $this->pagination->create_links());

        $this->render();
    }

    /**
     * The default method called. Typically displays an overview of this
     * controller's domain.
     * 
     * @return void
     */
    public function all()
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Load files
        $this->load->library('pagination');
        $this->load->config('pagination');

        // Get the requested items
        $segment      = $this->uri->segment( $this->uri->total_segments() );
        $offset       = $this->limit * (intval($segment) > 1 ? $segment - 1 : 0);
        $texceptions  = $this->texception_model
                             ->limit($this->limit, $offset)
                             ->find_all();

        // Pagination
        $pagination_config                = config_item('pagination.config');
        $pagination_config['base_url']    = site_url('admin/texceptions/all');
        $pagination_config['total_rows']  = $this->texception_model->count_all();
        $pagination_config['uri_segment'] = $this->uri->total_segments();
        $pagination_config['per_page']    = $this->limit; 
        $this->pagination->initialize($pagination_config);

        // Make vars available in views
        $this->setVar('texceptions', $texceptions);
        $this->setVar('pagination', $this->pagination->create_links());

        $this->render();
    }

    /**
     * Display time exceptions for review
     *
     * @uses $this->index()
     */
    public function review()
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Load files
        $this->load->library('pagination');
        $this->load->config('pagination');

        // Get the requested items
        $segment      = $this->uri->segment( $this->uri->total_segments() );
        $offset       = $this->limit * (intval($segment) > 1 ? $segment - 1 : 0);
        $texceptions  = $this->texception_model
                             ->where('supervisor', $this->authenticate->id())
                             ->limit($this->limit, $offset)
                             ->find_all();

        // Pagination
        $pagination_config                = config_item('pagination.config');
        $pagination_config['base_url']    = site_url('admin/texceptions/review');
        $pagination_config['total_rows']  = $this->texception_model->count_by('supervisor', $this->authenticate->id());
        $pagination_config['uri_segment'] = $this->uri->total_segments();
        $pagination_config['per_page']    = $this->limit; 
        $this->pagination->initialize($pagination_config);

        // Make vars available in views
        $this->setVar('texceptions', $texceptions);
        $this->setVar('pagination', $this->pagination->create_links());
        $this->setVar('supervisor_list', get_users_by_group('supervisor'));

        $this->render();
    }

    /**
     * Create a single time exception.
     *
     * @uses get_users_by_group() Gets all users belonging to the a given group.
     * @uses get_user_fullname()  Get user's first and last name as a string.
     * @uses datetime_to_input()  Formats a string for use with a value
     *                            attribute of a datetime-local input HTML tag.
     *                           
     * @return void
     */
    public function create()
    {
        // Require user to be an admin to access
        $this->restrictToGroups('agent');

        // Load files
        $this->load->helper('form');

        // Check for a $_POST
        if ($this->input->method() == 'post')
        {
            // Get the submitted data
            $post_data = $this->input->post();

            // Create the item
            if ($this->texception_model->insert($post_data) )
            {
                $this->setMessage('Time exception created.', 'success');
                redirect( site_url('admin/texceptions') );
            }

            $this->setMessage('Error creating time exception. '. $this->texception_model->error(), 'danger');
        }

        // Get data for views
        foreach (get_users_by_group('supervisor') as $option)
        {
            $supervisor_options[$option->id] = get_user_fullname($option->id);
        }
        
        // Make vars available in views
        $this->setVar('start_default',      datetime_to_input('-10 minutes'));
        $this->setVar('end_default',        datetime_to_input('now'));
        $this->setVar('supervisor',         $this->user_model->getMetaItem($this->authenticate->id(), 'supervisor'));        
        $this->setVar('supervisor_options', $supervisor_options);
        $this->setVar('snippets',           explode("\n", $this->user_model->getMetaItem($this->authenticate->id(), 'snippets')));

        $this->render();
    }

    /**
     * Display a single time exception.
     *
     * @param  integer $id The time exception ID.
     * @return void
     */
    public function show($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups('agent');

        // Get the requested $texception
        $texception = $this->texception_model->find($id);

        // Check $texception for an empty result
        if (! $texception)
        {
            $this->setMessage('Unable to find that time exception.', 'warning');
            redirect( site_url('admin/texceptions') );
        }

        // Make vars available in views
        $this->setVar('texception', $texception);

        $this->render();
    }

    /**
     * Display a single time exception for review.
     * 
     * @uses $this->show()
     *
     * @param  integer $id The time exception ID.
     * @return void
     */
    public function audit($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Get the requested $texception
        $texception = $this->texception_model->find($id);

        // Check $texception for an empty result
        if (! $texception)
        {
            $this->setMessage('Unable to find time exception.', 'warning');
            redirect( site_url('admin/texceptions/review') );
        }

        // Make vars available in views
        $this->setVar('texception', $texception);

        $this->render();
    }

    /**
     * Update a single time exception.
     *
     * @uses get_users_by_group() Gets all users belonging to the a given group.
     * @uses get_user_fullname()  Get user's first and last name as a string.
     * @uses datetime_to_input()  Formats a string for use with a value
     *                            attribute of a datetime-local input HTML tag.
     *                            
     * @param  integer $id The time exception ID.
     * @return void
     */
    public function update($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups('agent');

        // Load files
        $this->load->helper('form');
        $this->load->helper('inflector');

        // Check for a $_POST
        if ($this->input->method() == 'post')
        {
            // Get the submitted data
            $post_data = $this->input->post();

            // Update the item
            if ($this->texception_model->update($id, $post_data))
            {
                $this->setMessage('Time exception updated.', 'success');
                redirect( site_url('admin/texceptions') );
            }

            $this->setMessage('Error updating time exception. '. $this->texception_model->error(), 'danger');
        }

        // Get the requested $texception
        $texception = $this->texception_model->find($id);

        // Get data for views
        $texception->start = datetime_to_input($texception->start);
        $texception->end   = datetime_to_input($texception->end);
        foreach (get_users_by_group('supervisor') as $option)
        {
            $supervisor_options[$option->id] = get_user_fullname($option->id);
        }

        // Make vars available in views
        $this->setVar('texception', $texception);
        $this->setVar('supervisor_options', $supervisor_options);

        $this->render();
    }

    /**
     * Delete a single time exception.
     *
     * @param  integer $id The time exception ID.
     * @return void
     */
    public function delete($id)
    {
        // Delete the time exception
        if ($this->texception_model->delete($id))
        {
            $this->setMessage('Time exception deleted.', 'success');
            redirect( site_url('admin/texceptions') );
        }
        
        $this->setMessage('Error deleting time exception. '. $this->texception_model->error(), 'danger');
        redirect( site_url( 'admin/texceptions' ) );
    }

    /**
     * Approve a time exception.
     *
     * @param  integer $id The time exception ID.
     * @return void
     */
    public function approve($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Approve the time exception
        $data = 
        [
            'approved'    => 1,
            'approved_by' => $this->authenticate->id(),
            'approved_on' => date("Y-m-d H:i:s"),
        ];
        if ( $this->texception_model->update($id, $data, true) )
        {
            $this->setMessage('Time exception approved.', 'success');
            redirect( site_url('admin/texceptions/review') );
        }

        $this->setMessage('Error approving time exception. '. $this->texception_model->error(), 'danger');
        redirect( site_url( 'admin/texceptions/review' ) );
    }

    /**
     * Deny a time exception.
     *
     * @param  integer $id The time exception ID.
     * @return void
     */
    public function deny($id)
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Deny the time exception
        $data = 
        [
            'approved'    => 0,
            'approved_by' => $this->authenticate->id(),
            'approved_on' => date("Y-m-d H:i:s"),
        ];
        if ( $this->texception_model->update($id, $data, true) )
        {
            $this->setMessage('Time exception denied.', 'success');
            redirect( site_url('admin/texceptions/review') );
        }

        $this->setMessage('Error denying time exception. '. $this->texception_model->error(), 'danger');
        redirect( site_url( 'admin/texceptions/review' ) );
    }

    /**
     * Process all reviewed time exceptions.
     *
     * @return void
     */
    public function processReviewed()
    {
        // Require user to be an admin to access
        $this->restrictToGroups(['supervisor', 'administrator']);

        // Get current user
        $user = $this->authenticate->user();

        /*
         * Approved
         */
        
        $approved = $this->texception_model
                         ->where('supervisor', $user['id'])
                         ->where('approved', 1)
                         ->find_all();

        if ( ! empty($approved) )
        {
            // Email time exceptions
            $options['to'] = $user['email'];
            Mail::deliver('TexceptionMailer:processApproved', $approved, $options);

            // Delete time exceptions
            $wheres = array(
                'supervisor' => $user['id'],
                'approved'   => 1
            );
            if ( ! $this->texception_model->delete_by($wheres) )
            {
                $this->setMessage('Error processing reviewed time exceptions.', 'danger');
                redirect( site_url( 'admin/texceptions/review' ) );
            }
        }

        /*
         * Denied
         */

        $denied = $this->texception_model
                       ->where('supervisor', $user['id'])
                       ->where('approved', 0)
                       ->find_all();

        if ( ! empty($denied) )
        {
            foreach ($denied as $texception)
            {
                // Email time exception
                $created_by          = get_user_by_id($texception->created_by,    'email', false);
                $authorized_by       = get_user_by_id($texception->authorized_by, 'email', false);
                $options['to']       = $created_by->email;
                $options['reply-to'] = $user['email'];
                $options['cc']       = ! empty($authorized_by) ? $authorized_by->email : null;
                Mail::deliver('TexceptionMailer:processDenied', $texception, $options);

                // Delete time exception
                if ( ! $this->texception_model->delete($texception->id) )
                {
                    $this->setMessage('Error processing reviewed time exceptions.', 'danger');
                    redirect( site_url( 'admin/texceptions/review' ) );
                }
            }
        }

        $this->setMessage('All reviewed time exceptions have been processed.', 'success');
        redirect( site_url( 'admin/texceptions/review' ) );
    }

}
