<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * The application Admin controller
 *
 * Mainly responsible for displaying the dashboard.
 */
class Admin extends AdminController {

    /**
     * Initialize the Home Controller.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        parent::__construct();

        $this->benchmark->mark('Initialized Home Controller');
    }*/

    /**
     * The default method called. Typically displays an overview of this
     * controller's domain.
     * 
     * @return mixed Renders the webpage.
     */
    public function index()
    {
        // Load files
        $this->load->model('texception_model');

        $user = $this->authenticate->user();

        // Make vars available to theme and views
        if ( $this->authorize->inGroup('agent', $user['id']) )
        {
            $this->setVar('user_group',    'agent');
            $this->setVar('total',         $this->texception_model->count_by(['created_by' => $user['id'], 'deleted'  => 0]));
            $this->setVar('pending',       $this->texception_model->count_by(['created_by' => $user['id'], 'deleted'  => 0, 'approved' => null]));
            $this->setVar('approved',      $this->texception_model->count_by(['created_by' => $user['id'], 'deleted'  => 0, 'approved' => 1]));
            $this->setVar('denied',        $this->texception_model->count_by(['created_by' => $user['id'], 'deleted'  => 0, 'approved' => 0]));
        }
        else if ( $this->authorize->inGroup('supervisor', $user['id']) )
        {
            $this->setVar('user_group',    'supervisor');
            $this->setVar('reviewed',      $this->texception_model->count_by(['supervisor' => $user['id'], 'deleted'  => 0, 'approved' => 0, 'approved' => 1]));
            $this->setVar('unreviewed',    $this->texception_model->count_by(['supervisor' => $user['id'], 'deleted'  => 0, 'approved' => null]));
            $this->setVar('unprocessed',   $this->texception_model->count_by(['supervisor' => $user['id'], 'deleted'  => 0]));
        }

        // Display the webpage
        $this->render();
    }

}