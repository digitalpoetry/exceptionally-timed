<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Controller
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @inheritdoc
 */
class Home extends SiteController {

    /**
     * Initialize the Home Controller.
     *
     * @return void
     */
    /*public function __construct()
    {
        parent::__construct();

        $this->benchmark->mark('Initialized Home Controller');
    }*/

    /**
     * Index Page for this controller.
     *
     * @return mixed Renders the webpage.
     */
    public function index()
    {
        // Get current user id
        $user_id = $this->authenticate->id();

        // Supervisor
        if ( $this->authorize->inGroup('supervisor', $user_id) )
        {
            $jumbo_button = '<a class="btn btn-primary btn-lg" href="' . site_url('/admin/texceptions/review') . '" role="button">Review time exceptions</a>';
        }
        // Agent
        else if ( $this->authorize->inGroup('agent', $user_id) )
        {
            $jumbo_button = '<a class="btn btn-primary btn-lg" href="' . site_url('/admin/texceptions/create') . '" role="button">Submit an time exception</a>';
        }
        // Default
        else
        {
            $jumbo_button = '<a class="btn btn-primary btn-lg" href="' . site_url('/admin') . '" role="button">Dashboard</a>';
        }

        // Make vars available to theme and views
        $this->setVar('jumbo_button', $jumbo_button);

        $this->render();
    }
}
