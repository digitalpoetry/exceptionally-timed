<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Model
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 *
 * @todo  I10n strings in this file
 */


use Myth\Models\CIDbModel;

/**
 * Texception Model.
 */
class Texception_model extends CIDbModel {

    /**
     * Database table name for this model.
     * 
     * @var String
     */
    protected $table_name  = 'texceptions';

    /**
     * @var Boolean $soft_deletes Set true to enable soft deletes.
     */
    protected $soft_deletes = true;

    /**
     * If set to true, logs the user id for 'created_by', 'modified_by' and 
     * 'deleted_by'.
     * 
     * @var Boolean $log_user Set true to enable user logging.
     */
    protected $log_user = true;

    /**
     * @var array $before_insert Before Insert Callback, List of methods to call.
     */
    protected $before_insert = ['set_authorized_by'];

    /**
     * @var array $before_update Before Update Callback, List of methods to call.
     */
    protected $before_update = ['set_authorized_by'];

    /**
     * List of fields in the table. This can be set to avoid a database call if 
     * using $this->prep_data().
     * 
     * @var Array
     */
    protected $fields = [
        'id',         'created_by',  'authorized_by', 'supervisor',  'start', 
        'end',        'reason',      'approved',      'approved_by', 'approved_on', 
        'created_on', 'modified_on', 'deleted',       'deleted_by'
    ];

    /**
     * Protected, non-modifiable fields.
     * 
     * @var Array
     */
    protected $protected_attributes = [
        'id',
        'snippet',
        'submit'
    ];

    /**
     * An array of validation rules.
     * 
     * @var Array 
     */
    protected $validation_rules = [
        ['field' => 'authorized_by', 'label' => 'Authorized By', 'format' => 'username',  'rules' => 'trim|integer|max_length[9]'],
        ['field' => 'supervisor',    'label' => 'Supervisor',    'format' => 'username',  'rules' => 'trim|required|integer|max_length[9]'],
        ['field' => 'start',         'label' => 'Start',         'format' => 'datetime',  'rules' => 'trim|required|exact_length[16]'],
        ['field' => 'end',           'label' => 'End',           'format' => 'datetime',  'rules' => 'trim|required|exact_length[16]'],
        ['field' => 'reason',        'label' => 'Reason',        'format' => 'htmlchars', 'rules' => 'trim|required|min_length[3]|max_length[255]'],
    ];

    /**
     * Additional validation rules used during inserts only.
     * 
     * @var Array
     */
    protected $insert_validate_rules = [];


    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->benchmark->mark('Initialized Texceptions Model');
    }

    /**
     * Callback method for 'authorized_by'.
     * 
     * A callback method which unsets the 'authorized_by' field if its empty.
     * 
     * @param  array $data The post data
     * @return array The modified data fields
     */
    protected function set_authorized_by($data)
    {
        if (isset($data['fields']))
        {
            $data = $data['fields'];
        }

        if ( empty($data['authorized_by']) )
        {
            unset($data['authorized_by']);
        }

        return $data;
    }

}