<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Core
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Include the HMVC Router Class.
 *
 * @since   0.1.0 Shiny Things
 * @see     DigitalPoetry\CATT\ThirdParty\HMVC_Router
 */
require_once APPPATH .'third_party/HMVC/Router.php';

/**
 * @inheritdoc
 */
class CATT_Router extends HMVC_Router {}
