<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Core
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

use Myth\Navigation\MenuCollection;
use Myth\Navigation\MenuItem;

/**
 * @inheritdoc
 */
class CATT_Controller extends \Myth\Controllers\ThemedController {

    use \Myth\Auth\AuthTrait;

    /**
     * @var null \$theme Allows per-controller override of theme.
     */
    protected $theme = 'bootstrap';

    /**
     * @var string \$uikit The UIKit to make available to the template views.
     */
    protected $uikit = '\Myth\UIKits\Bootstrap';

    /**
     * Initialize Themed Controller.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Load files
        $this->load->helper(['view', 'menu']);
      
        // Setup authenticate and authorize libraries
        $this->setupAuthClasses();

        // Set the username for theme
        if ($this->authenticate->isLoggedIn())
        {
            // Build main menu
            $menu = MenuCollection::menu('user');
            $menu->addItem( new MenuItem('dashboard',     'Dashboard', site_url('/admin'),                  '', 10) );
            $menu->addItem( new MenuItem('users-profile', 'My Profile',   site_url('/admin/users/profile'), '', 20) );

            // Get the current user
            $user = $this->authenticate->user();

            // Make vars available to theme and views
            $this->setVar('menu_usernav', $menu->sortBy('order')->items());
            $this->setVar('username', $user['username']);
        }

        // Make vars available to theme and views
        $this->setVar('is_logged_in', $this->authenticate->isLoggedIn());
    }

}
