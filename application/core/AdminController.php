<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

use Myth\Route;
use Myth\Navigation\MenuCollection;
use Myth\Navigation\MenuItem;

/**
 * Indicates when we are in an admin area. Useful for modules and controllers.
 * 
 * @var boolean Defined when in admin area.
 */
define('IN_ADMIN', true);

/**
 * Admin Controller Library.
 */
class AdminController extends CATT_Controller
{
    /**
     * Which theme to use for admin area.
     *
     * @var string
     */
    protected $layout = 'admin';

    /**
     * @var int $limit The number of rows to show when paginating results.
     */
    protected $limit = 20;

    /**
     * Initialize Admin Controller.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Require users to be logged in to access
        $this->restrict( Route::named('login') );

        // Load files
        $this->load->library('authorization');

        // Get current user id
        $user_id = $this->authenticate->id();
        
        // Build main menu
        $menu = MenuCollection::menu('admin');
        $menu->addItem( new MenuItem('dashboard-header',   'Dashboard',       '',                                    '', 10) );
        $menu->addItem( new MenuItem('dashboard',          'My Dashboard',    site_url('/admin'),                    '', 11) );
        $menu->addItem( new MenuItem('texceptions-header', 'Time Exceptions', '',                                    '', 20) );
        $menu->addItem( new MenuItem('users-header',       'Users',           '',                                    '', 30) );
        $menu->addItem( new MenuItem('users-profile',      'My Profile',      site_url('/admin/users/profile'),      '', 33) );
        // Agent menu links
        if ( $this->authorize->inGroup('agent', $user_id) )
        {
            $menu->addItem( new MenuItem('texceptions',        'My Exceptions', site_url('/admin/texceptions'),        '', 21) );
            $menu->addItem( new MenuItem('texceptions-create', 'New Exception', site_url('/admin/texceptions/create'), '', 22) );
        }
        // Supervisor menu links
        if ( $this->authorize->inGroup('supervisor', $user_id) )
        {
            $menu->addItem( new MenuItem('texceptions-review', 'Review Exceptions', site_url('/admin/texceptions/review'), '', 23) );
        }
        // Administrator menu links
        if ( $this->authorize->inGroup('administrator', $user_id) )
        {
            $menu->addItem( new MenuItem('texceptions-all', 'All Exceptions', site_url('/admin/texceptions/all'), '', 23) );
        }
        // Administrator or Supervisor menu links
        if ( $this->authorize->inGroup(['administrator', 'supervisor'], $user_id) )
        {
            $menu->addItem( new MenuItem('users',           'All Users',      site_url('/admin/users'),           '', 31) );
            $menu->addItem( new MenuItem('users-create',    'New User',       site_url('/admin/users/create'),    '', 32) );
        }

        // Make vars available to theme and views
        $this->setVar('menu_sidenav', $menu->sortBy('order')->items());
        $this->setVar('containerClass', 'container-fluid');
        $this->setVar('navbar_style', 'navbar-fixed-top');

    }

}
