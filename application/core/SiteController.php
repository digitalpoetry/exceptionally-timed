<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

use Myth\Navigation\MenuCollection;
use Myth\Navigation\MenuItem;

/**
 * Site Controller Library.
 */
class SiteController extends CATT_Controller
{
    /**
     * @var $layout Per-controller override of the current layout file.
     */
    //protected $layout = 'two_left';

    /**
     * Initialize Site Controller.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Get current user id
        $user_id = $this->authenticate->id();

        // Build main menu
        $menu = MenuCollection::menu('admin');
        // Agent menu links
        if ( $this->authorize->inGroup('agent', $user_id) )
        {
            $menu->addItem( new MenuItem('texceptions', 'Time Exceptions', site_url('/admin/texceptions'),        '', 21) );
        }
        // Supervisor menu links
        if ( $this->authorize->inGroup('supervisor', $user_id) )
        {
            $menu->addItem( new MenuItem('texceptions-review', 'Review Exceptions', site_url('/admin/texceptions/review'), '', 23) );
        }

        // Make vars available to theme and views
        $this->setVar('menu_topnav', $menu->sortBy('order')->items());
    }

}
