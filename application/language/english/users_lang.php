<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\I10N
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

$lang['auth.create_account']   = 'Add User';

$lang['form_username_placeholder']   = 'username';
$lang['form_first_mane_placeholder'] = 'First name';
$lang['form_last_mane_placeholder']  = 'Last name';
