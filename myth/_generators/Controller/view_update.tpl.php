<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="@= site_url('<?= $lc_name ?>/update/' . $<?= $lc_name ?>->id) ?>" class="btn btn-danger" role="button" onclick="return confirm('Delete this item?');">Delete <?= $single_name ?></a>
        <a href="<?= site_url('admin/users/profile') ?>" class="btn btn-default" role="button">Cancel</a>
    </div>
    <h2 >Update <?= $single_name ?></h2>
</div><!-- /.page-header -->

@= form_open('', array('class' => 'form-horizontal')); ?>

<?= $uikit->row([], function() use($uikit, $fields)
{
    $sizes = [
        's' => 12,
        'm' => 6,
        'l' => 4
    ];

    echo $uikit->column( [ 'sizes' => $sizes ], function () use ( $uikit, $fields )
    {
        foreach ( $fields as $field )
        {
            $name = $field['name'];
            $label = ucwords(humanize($name));

            echo $uikit->inputWrap( $label, NULL, function () use ( $uikit, $field )
            {
                echo "\t\t\t<!-- {$label} -->\n";

                switch ( $field['type'] )
                {
                    case 'text':
                        echo '<input type="text" name="' . $name . '" class="form-control" value="@= set_value(\'' . $name . '\', \$' . $lc_name . '->' . $name . ' ) ?>" />';
                        break;
                    case 'number':
                        echo '<input type="number" name="' . $name . '" class="form-control" value="@= set_value(\'' . $name . '\', \$' . $lc_name . '->' . $name . ' ) ?>" />';
                        break;
                    case 'date':
                        echo '<input type="date" name="' . $name . '" class="form-control" value="@= set_value(\'' . $name . '\', \$' . $lc_name . '->' . $name . ' ) ?>" />';
                        break;
                    case 'datetime':
                        echo '<input type="datetime" name="' . $name . '" class="form-control" value="@= set_value(\'' . $name . '\', \$' . $lc_name . '->' . $name . ' ) ?>" />';
                        break;
                    case 'time':
                        echo '<input type="time" name="' . $name . '" class="form-control" value="@= set_value(\'' . $name . '\', \$' . $lc_name . '->' . $name . ' ) ?>" />';
                        break;
                    case 'textarea':
                        echo '<textarea  class="form-control" name="' . $name . '">@= set_value(\'' . $name . '\') ?></textarea>';
                        break;
                }

                echo "\n";
            } );
        }
    } );
} );
?>

<!-- Submit -->
<div class="form-group">
    <div class="col-sm-offset-2">
        <input type="submit" name="submit" class="btn btn-primary" value="Update <?= $single_name ?>" />
        &nbsp;or&nbsp;
        <a href="@= site_url('<?= $lc_name ?>') ?>">Cancel</a>
    </div>
</div>

@= form_close(); ?>
