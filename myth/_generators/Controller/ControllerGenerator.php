<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Forge
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

use Myth\CLI;

/**
 * Controller Generator Class.
 */
class ControllerGenerator extends \Myth\Forge\BaseGenerator {

	protected $options = [
		'cache_type'   => 'null',
		'backup_cache' => 'null',
		'ajax_notices' => 'true',
		'lang_file'    => 'null',
		'model'        => null,
		'themed'       => false,
		'base_class'   => 'BaseController',
		'base_path'    => 'Myth\Controllers\\'
	];

	/**
	 * Generates a Controller.
	 * 
	 * @param  array   $segments
	 * @param  boolean $quiet
	 * @return boolean
	 */
	public function run( $segments = [ ], $quiet = false )
	{
		$name = array_shift( $segments );

		if ( empty( $name ) )
		{
			$name = CLI::prompt( 'Controller name' );
		}

		// Format to CI Standards
		$name = ucfirst( $name );

		if ( $quiet === false )
		{
			$this->collectOptions( $name );
		}
		else
		{
			$this->quietSetOptions( $name );
		}

		$data = [
			'controller_name' => $name,
			'today'           => date( 'Y-m-d H:ia' )
		];

		$data = array_merge( $data, $this->options );

		if ($data['themed'] == 'y' || $data['themed'] === true)
		{
			$data['base_class'] = 'CATT_Controller';
		}

		$destination = $this->determineOutputPath( 'controllers' ) . $name . '.php';

		if ( ! $this->copyTemplate( 'controller', $destination, $data, $this->overwrite ) )
		{
			CLI::error( 'Error creating new files' );
		}

		if ( CLI::option( 'create_views' ) && $this->options['themed'] == 'y' )
		{
			$this->createViews( $name );
		}

		return true;
	}

	/**
	 * QuietSetOptions
	 * 
	 * @param  string $model_name
	 * @param  array  $options
	 * @return void
	 */
	protected function quietSetOptions( $name )
	{
		$options = CLI::getOptions();

		if ( ! empty( $options['model'] ) )
		{
			$this->options['model'] = $options['model'];

			// Format per CI
			if ( ! empty( $this->options['model'] ) && substr( $this->options['model'], - 6 ) !== '_model' )
			{
				$this->options['model'] .= '_model';
			}
			$this->options['model'] = ! empty( $this->options['model'] ) ? ucfirst( $this->options['model'] ) : null;

			$this->options['themed'] = 'y';
		}


	}

	/**
	 * CollectOptions
	 * 
	 * @param  string $name
	 * @return void
	 */
	protected function collectOptions( $name )
	{
		$options = CLI::getOptions();

		// Model?
		$this->options['model'] = empty( $options['model'] ) ?
			CLI::prompt( 'Model Name? (empty is fine)' ) :
			$options['model'];

		// Format per CI
		if ( ! empty( $this->options['model'] ) && substr( $this->options['model'], - 6 ) !== '_model' )
		{
			$this->options['model'] .= '_model';
		}
		$this->options['model'] = ! empty( $this->options['model'] ) ? ucfirst( $this->options['model'] ) : null;

		// If we're using a model, then force the use of a themed controller.
		if ( ! empty( $this->options['model'] ) )
		{
			$options['themed'] = 'y';
		}

		// Themed Controller?
		$this->options['themed'] = empty( $options['themed'] ) ?
			CLI::prompt( 'Is a Themed Controller?', [ 'y', 'n' ] ) :
			$options['themed'];

		$this->options['themed'] = $this->options['themed'] == 'y' ? true : false;

		if ( $this->options['themed'] )
		{
			$this->options['base_class'] = 'CATT_Controller';
		}
	}

	/**
	 * Generates the standard views for our CRUD methods.
	 */
	protected function createViews( $name )
	{
		$this->load->helper( 'inflector' );

		$data = [
			'name'        => $name,
			'lower_name'  => strtolower($name),
			'single_name' => singular($name),
			'plural_name' => plural($name),
			'fields'      => $this->prepareFields()
		];

		$subfolder = empty( $this->module ) ? '/' . strtolower($name) : '/'. $data['lower_name'];

		// Index
		$destination = $this->determineOutputPath( 'views' . $subfolder ) . 'index.php';
		$this->copyTemplate( 'view_index', $destination, $data, $this->overwrite );

		// Create
		$destination = $this->determineOutputPath( 'views' . $subfolder ) . 'create.php';
		$this->copyTemplate( 'view_create', $destination, $data, $this->overwrite );

		// Show
		$destination = $this->determineOutputPath( 'views' . $subfolder ) . 'show.php';
		$this->copyTemplate( 'view_show', $destination, $data, $this->overwrite );

		// Index
		$destination = $this->determineOutputPath( 'views' . $subfolder ) . 'update.php';
		$this->copyTemplate( 'view_update', $destination, $data, $this->overwrite );
	}

	/**
	 * Grabs the fields from the CLI options and gets them ready for
	 * use within the views.
	 */
	protected function prepareFields()
	{
		$fields = CLI::option( 'fields' );

		if ( empty( $fields ) )
		{
            // If we have a model, we can get our fields from there
            if (! empty($this->options['model']))
            {
                $fields = $this->getFieldsFromModel( $this->options['model'] );

                if (empty($fields))
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
		}

		$fields = explode( ' ', $fields );

		$new_fields = [ ];

		foreach ( $fields as $field )
		{
			$pop = [ null, null, null ];
			list( $field, $type, $size ) = array_merge( explode( ':', $field ), $pop );
			$type = strtolower( $type );

			// Ignore list
			if (in_array($field, ['created_on', 'modified_on']))
			{
				continue;
			}

			// Strings
			if (in_array($type, ['char', 'varchar', 'string']))
			{
				$new_fields[] = [
					'name'  => $field,
					'type'  => 'text'
				];
			}

			// Textarea
			else if ($type == 'text')
			{
				$new_fields[] = [
					'name'  => $field,
					'type'  => 'textarea'
				];
			}

			// Number
			else if (in_array($type, ['tinyint', 'int', 'bigint', 'mediumint', 'float', 'double', 'number']))
			{
				$new_fields[] = [
					'name'  => $field,
					'type'  => 'number'
				];
			}

			// Date
			else if (in_array($type, ['date', 'datetime', 'time']))
			{
				$new_fields[] = [
					'name'  => $field,
					'type'  => $type
				];
			}
		}

		return $new_fields;
	}

    private function getFieldsFromModel( $model )
    {
        $this->load->model($model);

        if (! $this->db->table_exists( $this->$model->table() ))
        {
            return '';
        }

        $fields = $this->db->field_data( $this->$model->table() );

        $return = '';

        // Prepare the fields in a string format like
        // it would have been passed on the CLI
        foreach ($fields as $field)
        {
            $temp = $field->name .':'. $field->type;

            if (! empty($field->max_length))
            {
                $temp .= ':'. $field->max_length;
            }

            $return .= ' '. $temp;
        }

        return $return;
    }

}
