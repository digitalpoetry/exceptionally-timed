<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Forge
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/*
 * Collect our options
 */
$model_string       = $model ? "'{$model}'" : 'null';
$lc_model        = trim( strtolower($model_string), "' " );
$lc_model_esc    = strpos($lc_model, 'null') !== false ? 'null' : "'{$lc_model}'";
$lc_controller   = strtolower($controller_name);
$lc_single_name  = strtolower($single_name);
$lc_plural_name  = strtolower($plural_name);
foreach ($fields as $field)
{
   $field_list[] = "'{$field['name']}' => '{ucwords(humanize($field['name']))}',"
}
$field_list = implode("\n\t\t\t", $field_list);


/*
 * Build our Methods
 * 
 * All methods assume that if we have a model given to use, we're
 * extending ThemedController since that is the most common use case.
 */

/*
 * Index
 */
$index_method = '';

if (! empty($model) )
{
    $index_method = <<<EOD
// Load files
        \$this->load->library('table');

        // Get the requested {$plural_name}
        \$offset = \$this->uri->segment( \$this->uri->total_segments() );
        \${$lc_plural_name} = \$this->{$lc_model}->limit(\$this->limit, \$offset)
                     ->as_array()
                     ->find_all();

        // Make\$fields available in views
        \$this->setVar('fields', array(
            {$field_list}
        ));

        // Make\${single_name} available to theme and view
        \$this->setVar('{$lc_plural_name}', \${$lc_plural_name});

\t\t
EOD;
}

/*
 * Create
 */
$create_method = '';

if (! empty($model))
{
    $create_method = <<<EOD
// Load files
        \$this->load->helper('form');
        \$this->load->helper('inflector');

        // Check for form submission
        if (\$this->input->method() == 'post')
        {
            // Get the submitted data
            \$post_data = \$this->input->post();

            // If {$lc_single_name} is saved
            if (\$this->{$lc_model}->insert(\$post_data) )
            {
                // Set a response
                \$this->setMessage('Successfully created {$lc_single_name}.', 'success');
                // Redirect the page
                redirect( site_url('{$lc_controller}') );
            }

            // Set a response
            \$this->setMessage('Error creating {$lc_single_name}. '. \$this->{$lc_model}->error(), 'danger');
        }

        // Make\$fields available in views
        \$this->setVar('fields', array(
            {$field_list}
        ));

\t\t
EOD;
}

/*
 * Show
 */
$show_method = '';

if (! empty($model))
{
    $show_method = <<<EOD
// Load files
        \$this->load->library('table');

        // Get the requested {$lc_single_name}
        \${$lc_single_name} = \$this->{$lc_model}->find(\$id);

        // If {$lc_single_name} is not found
        if (! \${$lc_single_name})
        {
            // Set a response
            \$this->setMessage('Unable to find that {$lc_single_name}.', 'warning');
            // Redirect the page
            redirect( site_url('{$lc_controller}') );
        }
        
        // Make\$fields available in views
        \$this->setVar('fields', array(
            {$field_list}
        ));

        // Make\${single_name} available to theme and view
        \$this->setVar('{$lc_single_name}', \${$lc_single_name});

\t\t
EOD;
}

/*
 * Update
 */
$update_method = '';

if (! empty($model))
{
    $update_method = <<<EOD
// Load files
        \$this->load->helper('form');
        \$this->load->helper('inflector');

        // Check for form submission
        if (\$this->input->method() == 'post')
        {
            // Get the submitted data
            \$post_data = \$this->input->post();

            // If {$lc_single_name} is updated
            if (\$this->{$lc_model}->update(\$id, \$post_data))
            {
                // Set a response
                \$this->setMessage('Successfully updated {$lc_single_name}.', 'success');
                // Redirect the page
                redirect( site_url('{$lc_controller}') );
            }

            // Set a response
            \$this->setMessage('Error updating {$lc_single_name}. '. \$this->{$lc_model}->error(), 'danger');
        }

        // Get the requested {$lc_single_name}
        \${$lc_single_name} = \$this->{$lc_model}->find(\$id);

        // Make\${single_name} available to theme and view
        \$this->setVar('{$lc_single_name}', \${$lc_single_name});

\t\t
EOD;
}

/*
 * Delete
 */
$delete_method = '';

if (! empty($model))
{
    $delete_method = <<<EOD
// If {$lc_single_name} is deleted
        if (\$this->{$lc_model}->delete(\$id))
        {
            // Set a response
            \$this->setMessage('Successfully deleted {$lc_single_name}.', 'success');
            // Redirect the page
            redirect( site_url('{$lc_controller}') );
        }

        // Set a response
        \$this->setMessage('Error deleting {$lc_single_name}. '. \$this->{$lc_model}->error(), 'danger');
        // Redirect the page
        redirect( site_url( '{$lc_controller}' ) );
\t\t
EOD;
}


/**
 * Additional class properties for themed controllers.
 */
if ($themed)
{
    $index_method   .= "\$this->render();";
    $create_method  .= "\$this->render();";
    $show_method    .= "\$this->render();";
    $update_method  .= "\$this->render();";

    $properties = "
    /**
     * @var null \$layout Per-controller override of the current layout file.
     */
    protected \$layout = null;

    /**
     * @var integer \$limit The number of {$lc_plural_name} to show when
     *                      paginating results.
     */
    protected \$limit = 25;
";
}

/*
 * The Template
 */
echo "<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Controller
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

use {$base_path}{$base_class};

/**
 * {$controller_name} Controller.
 *
 * @uses {$base_path}{$base_class} The base controller class.
 */
class {$controller_name} extends {$base_class} {

    /**
     * @var string $cache_type The type of caching to use.
     */
    protected \$cache_type = {$cache_type};

    /**
     * @var string $backup_cache The backup type of caching to use.
     */
    protected \$backup_cache = {$backup_cache};

    /**
     * @var string $language_file If set, this language file will be autoloaded.
     */
    protected \$language_file = {$lang_file};

    /**
     * @var string $model_file If set, this model file will be autoloaded.
     */
    protected \$model_file = {$lc_model_esc};

    {$properties}

    /**
     * The {$lc_single_name} controller default method. Shows a list of all {$lc_plural_name}.
     *
     * @return void
     */
    public function index()
    {
        {$index_method}
    }

    /**
     * Create a single {$lc_single_name}.
     *
     * @return void
     */
    public function create()
    {
        {$create_method}
    }

    /**
     * Displays a single {$lc_single_name}.
     *
     * @param  integer \$id The {$lc_single_name} ID.
     * @return void
     */
    public function show(\$id)
    {
        {$show_method}
    }

    /**
     * Updates a single {$lc_single_name}.
     *
     * @param  integer \$id The {$lc_single_name} ID.
     * @return void
     */
    public function update(\$id)
    {
        {$update_method}
    }

    /**
     * Deletes a single {$lc_single_name}
     *
     * @param  integer \$id The {$lc_single_name} ID.
     * @return void
     */
    public function delete(\$id)
    {
        {$delete_method}
    }

}
";
