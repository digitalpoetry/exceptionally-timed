<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="@= site_url('<?= $lc_name ?>/create') ?>" class="btn btn-primary" role="button">New <?= $single_name ?></a>
        <?= $uikit->buttonLink("New {$single_name}", "@= site_url('<?= $lc_name ?>/create') ?>", 'primary') ?>
        <?= $uikit->buttonLink("New {$single_name}", "{$lc_name}/create", 'primary') ?>
        @= $uikit->buttonLink("New {$single_name}", "{$lc_name}/create", 'primary') ?>
    </div>
    <h2 ><?= $plural_name ?></h2>
</div><!-- /.page-header -->

@php if ( ! empty($<?= $lc_name ?>) && is_array($<?= $lc_name ?>) && count($<?= $lc_name ?>) ) : ?>

    <div class="table table-responsive">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    @php foreach ($fields as $name => $label) : ?>
                    <th>@= $label ?></th>
                    @php endforeach; ?>
                    <th class="col-sm-2">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php foreach ($<?= $lc_name ?>s as $<?= $lc_name ?>) : ?>
                <tr>
                    @php foreach ($fields as $name => $label) : ?>
                    <td>@= $<?= $lc_name ?>->$name ?> ?></td>
                    @php endforeach; ?>
                    <td class="table-actions col-sm-2">
                        <a href="@= site_url('<?= $lc_name ?>/show/'   . $<?= $lc_name ?>->id) ?>" class="text-primary">View</a> |
                        <a href="@= site_url('<?= $lc_name ?>/update/' . $<?= $lc_name ?>->id) ?>" class="text-info">Edit</a> |
                        <a href="@= site_url('<?= $lc_name ?>/delete/' . $<?= $lc_name ?>->id) ?>" class="text-danger" onclick="return confirm('Delete this item?');">Delete</a>
                    </td>
                </tr>
                @php endforeach; ?>
            </tbody>
        </table>
    </div><!-- /.table-responsive -->

@php else : ?>

    @= $uikit->notice('Unable to find any <?= $plural_name ?>.', 'warning'); ?>

@php endif; ?>
