<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><div class="page-header">
    <h2>New <?= $single_name ?></h2>
</div><!-- /.page-header -->

@= form_open('', array('class' => 'form-horizontal')); ?>

<?= $uikit->row([], function() use($uikit, $fields)
{
    $sizes = [
        's' => 12,
        'm' => 6,
        'l' => 4
    ];

    echo $uikit->column(['sizes' => $sizes], function() use($uikit, $fields)
    {
        foreach ($fields as $field)
        {
            $name = $field['name'];
            $label = ucwords(humanize($name));

            echo $uikit->inputWrap( $label, null, function() use($uikit, $field)
            {
                echo "\t\t\t<!-- {$label} -->\n";

                switch ($field['type'])
                {
                    case 'text':
                        echo '<input type="text" class="form-control" name="' . $name . '" value="@= set_value(\'' . $name . '\' ?>" />';
                        break;
                    case 'number':
                        echo '<input type="number" class="form-control" name="' . $name . '" value="@= set_value(\'' . $name . '\' ?>" />';
                        break;
                    case 'date':
                        echo '<input type="date" class="form-control" name="' . $name . '" value="@= set_value(\'' . $name . '\' ?>" />';
                        break;
                    case 'datetime':
                        echo '<input type="datetime" class="form-control" name="' . $name . '" value="@= set_value(\'' . $name . '\' ?>" />';
                        break;
                    case 'time':
                        echo '<input type="time" class="form-control" name="' . $name . '" value="@= set_value(\'' . $name . '\' ?>" />';
                        break;
                    case 'textarea':
                        echo '<textarea name="' . $name . '" class='form-control'>@= set_value(\'' . $name . '\' ?></textarea>';
                        break;
                }

                echo "\n";
            } );
        }
    });

}); ?>

    <!-- Submit -->
    <div class="form-group">
        <div class="col-sm-offset-2">
            <input type="submit" class="btn btn-primary" name="submit" value="Update <?= $single_name ?>" />
            &nbsp;or&nbsp;
            <a href="@= site_url('<?= $lc_name ?>') ?>">Cancel</a>
        </div>
    </div>


@= form_close(); ?>
