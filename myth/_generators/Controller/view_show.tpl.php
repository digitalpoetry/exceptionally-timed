<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><div class="page-header">
    <div class="content-toolbar pull-right">
        <a href="@= site_url('<?= $lc_name ?>/update/' . $<?= $lc_name ?>->id) ?>" class="btn btn-primary" role="button">Edit <?= $single_name ?></a>
        <a href="@= site_url('<?= $lc_name ?>/update/' . $<?= $lc_name ?>->id) ?>" class="btn btn-danger" role="button" onclick="return confirm('Delete this item?');">Delete <?= $single_name ?></a>
    </div>
    <h2>View <?= $single_name ?></h2>
</div><!-- /.page-header -->

<div class="table table-responsive">
    <table class="table table-striped table-responsive">
	    <tbody>
	    @php foreach ($fields as $name => $label) : ?>
        <tr>
            <th class="col-sm-2">@= $label ?></th>
	        <td>@= $<?= $lc_name ?>->$name ?> ?></td>
        </tr>
	    @php endforeach; ?>
	    </tbody>
	</table>
</div><!-- /.table-responsive -->
