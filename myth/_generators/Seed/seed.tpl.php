<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Forge
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/*
 * The Template
 */
echo "<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Seeder
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Class {$seed_name}
 */
class {$seed_name} extends Seeder {

    /**
     * Runs the Seeder.
     *
     * @return void
     */
    public function run()
    {
        /*
            Here, you can do anything you need to do to get your
            database in the desired state. The following tools are ready for you:

                \$this->ci       // Full CI superobject
                \$this->db       // Database object
                \$this->dbforge  // DatabaseForge instance

            You can split your seeds into multiple files.
            To run them, use the call() method.

                \$this->call('UserSeeder');
         */
    }

}
";
