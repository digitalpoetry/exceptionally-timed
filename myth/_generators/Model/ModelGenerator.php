<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Forge
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

use Myth\CLI;

class ModelGenerator extends \Myth\Forge\BaseGenerator {

    protected $options = [
        'fields'            => '',
        'table_name'        => '',
        'primary_key'       => '',
        'set_created'       => true,
        'set_modified'      => true,
        'created_field'     => 'created_on',
        'modified_field'    => 'modified_on',
        'date_format'       => 'datetime',
        'log_user'          => true,
        'created_by_field'  => 'created_by',
        'modified_by_field' => 'modified_by',
        'deleted_by_field'  => 'deleted_by',
        'use_soft_deletes'  => true,
        'soft_delete_key'   => 'deleted',
        'protected'         => [],
        'return_type'       => 'object',
        'return_insert_id'  => true,
        'before_insert'     => '',
        'after_insert'      => '',
        'before_update'     => '',
        'after_update'      => '',
        'before_find'       => '',
        'after_find'        => '',
        'before_delete'     => '',
        'after_delete'      => '',
        'rules'             => '[]' /* comment this out? */
    ];


    public function run( $segments = [ ], $quiet = false )
    {
        $name = array_shift( $segments );

        $options = CLI::getOptions();

        $this->options['table_name'] = array_shift( $segments );

        if ( empty( $name ) )
        {
            $name = CLI::prompt( 'Model name' );
        }

        // Format to CI Standards
        if ( substr( $name, - 6 ) !== '_model' )
        {
            $name .= '_model';
        }
        $name = ucfirst( $name );

        if ( $quiet === false )
        {
            $this->collectOptions( $name, $options );
        }
        else
        {
            $this->quietSetOptions( $name, $options );
        }

        // Merge protected fields option from user input
        if ( isset($options['protected']) )
        {
            $this->options['protected'] = array_unique( array_merge( $this->options['protected'], explode(' ', $options['protected']) ) );
        }

        // Add callbacks options from user input
        $cb_options = ['before_insert', 'after_insert', 'before_update', 'after_update', 'before_find', 'after_find', 'before_delete', 'after_delete'];
        foreach ($cb_options as $cb_op) {
            $this->options[$cb_op] = isset($options[$cb_op]) ?
                $options[$cb_op] :
                $this->options[$cb_op];
        }

        $data = [
            'model_name' => $name,
            'today'      => date( 'Y-m-d H:ia' )
        ];

        $data = array_merge( $data, $this->options );

        $destination = $this->determineOutputPath( 'models' ) . $name . '.php';

        if ( ! $this->copyTemplate( 'model', $destination, $data, $this->overwrite ) )
        {
            CLI::error( 'Error creating new files' );
        }

        return true;
    }

    /*
     * Customizes our settings
     */
    protected function collectOptions( $model_name, $options=[] )
    {
        // Load Helper
        $this->load->helper( 'inflector' );

        // Table Name
        if ( empty( $this->options['table_name'] ) )
        {
            $this->options['table_name'] = empty( $options['table'] ) ?
                CLI::prompt( 'Table name', plural( strtolower( str_replace( '_model', '', $model_name ) ) ) ) :
                $options['table'];
        }

        // Fields - set from table first, otherwise pull from $options['fields']
        $this->options['fields'] = $this->table_info( $this->options['table_name'], $options );

        // Reset protected field names
        $this->options['protected'] = [ ];

        // Primary Key
        if ( empty( $this->options['primary_key'] ) )
        {
            $this->options['primary_key'] = empty( $options['primary_key'] ) ?
                CLI::prompt( 'Primary Key', 'id' ) :
                $options['primary_key'];
        }

        // Add primary key to the protected fields array
        $this->options['protected'][] = $this->options['primary_key'];

        // Set Created?
        if ( empty( $options['set_created'] ) )
        {
            $ans = CLI::prompt( 'Set Created date? ', ['y', 'n'] );
            if ( $ans == 'n' )
            {
                $this->options['set_created'] = false;
            }
        }

        // Set Modified?
        if ( empty( $options['set_modified'] ) )
        {
            $ans = CLI::prompt( 'Set Modified date? ', ['y', 'n'] );
            if ( $ans == 'n' )
            {
                $this->options['set_modified'] = false;
            }
        }

        // Log User?
        if ( empty( $options['log_user'] ) )
        {
            $ans = CLI::prompt( 'Log User actions? ', ['y', 'n'] );
            if ( $ans == 'n' )
            {
                $this->options['log_user'] = false;
            }
        }

        // Soft Deletes?
        if ( empty( $options['use_soft_deletes'] ) )
        {
            $ans = CLI::prompt( 'Use Soft Deletes? ', ['y', 'n'] );
            if ( $ans == 'n' )
            {
                $this->options['use_soft_deletes'] = false;
            }
        }

        // Return Insert IDs?
        if ( empty( $options['return_insert_id'] ) )
        {
            $ans = CLI::prompt( 'Return Insert IDs? ', ['y', 'n'] );
            if ( $ans == 'n' )
            {
                $this->options['return_insert_id'] = false;
            }
        }

        // Date Format
        $this->options['date_format'] = empty( $options['date_format'] ) ?
            CLI::prompt( 'Date Format? ', ['datetime', 'date', 'int'] ) :
            $options['date_format'];

        // Return Type
        $this->options['return_type'] = empty( $options['return_type'] ) ?
            CLI::prompt( 'Return Type? ', ['object', 'array'] ) :
            $options['return_type'];

        // Created On Field?
        if ( $this->options['set_created'] == true )
        {
            $this->options['created_field'] = empty( $options['created_field'] ) ?
                CLI::prompt( 'Created On Field?', $this->options['created_field'] ) :
                $options['created_field'];
            $this->options['protected'][] = $this->options['created_field'];
        }

        // Modified On Field?
        if ( $this->options['set_modified'] == true )
        {
            $this->options['modified_field'] =  empty( $options['modified_field'] ) ?
                CLI::prompt( 'Modified On Field?', $this->options['modified_field'] ) :
                $options['modified_field'];
            $this->options['protected'][] = $this->options['modified_field'];
        }

        // Log User Fields
        if ( $this->options['log_user'] == true )
        {            
            // Created By Field?
            $this->options['created_by_field'] = empty( $options['created_by_field'] ) ?
                CLI::prompt( 'Created By Field? Use default if none. ', $this->options['created_by_field'] ) :
                $options['created_by_field'];
            if ( in_array( $this->options['created_by_field'], $this->options['field_names'] ) )
            {
                $this->options['protected'][]= $this->options['created_by_field'];
            }
            
            // Modified By Field?
            $this->options['modified_by_field'] = empty( $options['modified_by_field'] ) ?
                CLI::prompt( 'Modified By Field? Use default if none. ', $this->options['modified_by_field'] ) :
                $options['modified_by_field'];
            if ( in_array( $this->options['modified_by_field'], $this->options['field_names'] ) )
            {
                $this->options['protected'][]= $this->options['modified_by_field'];
            }
            
            // Deleted By Field?
            $this->options['deleted_by_field'] = empty( $options['deleted_by_field'] ) ?
                CLI::prompt( 'Deleted By Field? Use default if none. ', $this->options['deleted_by_field'] ) :
                $options['deleted_by_field'];
            if ( in_array( $this->options['deleted_by_field'], $this->options['field_names'] ) )
            {
                $this->options['protected'][]= $this->options['deleted_by_field'];
            }
        }

        // Soft Deletes
        if ( $this->options['use_soft_deletes'] == true )
        {            
            // Soft Delete Key?
            $this->options['soft_delete_key'] = empty( $options['soft_delete_key'] ) ?
                CLI::prompt( 'Soft Delete Key?', $this->options['soft_delete_key'] ) :
                $options['soft_delete_key'];
            $this->options['protected'][] = $this->options['soft_delete_key'];
        }
    }

    protected function quietSetOptions( $model_name, $options=[] )
    {
        // Load Helper
        $this->load->helper( 'inflector' );

        // Table Name
        if (empty($this->options['table_name']))
        {
            $this->options['table_name'] = empty( $options['table'] ) ?
                plural( strtolower( str_replace( '_model', '', $model_name ) ) ) :
                $options['table'];
        }

        // Set fields from table first, otherwise pull from $options['fields']
        $this->options['fields'] = $this->table_info( $this->options['table_name'], $options );

        // Primary Key
        if ( empty( $this->options['primary_key'] ) )
        {
            $this->options['primary_key'] = ! empty( $options['primary_key'] ) ?
                $options['primary_key'] :
                'id';
        }

        // Add primary key to the protected fields array
        $this->options['protected'][] = $this->options['primary_key'];
    }

    /**
     * Get the structure and details for the fields in the specified DB table
     *
     * @param string $table_name Name of the table to check
     *
     * @return mixed An array of fields or false if the table does not exist
     */
    protected function table_info( $table_name, $options=[] )
    {
        $this->load->database();

        // Check whether the table exist
        if ( ! $this->db->table_exists( $table_name ) )
        {
            // Check whether fields from user input exist
            if (empty($options['fields']))
            {
                // No fields to use, return false
                return false;
            }

            // Use fields from user input
            $fields = $this->parseFieldString($options['fields']);
        }
        else
        {
            // Use fields from table
            $fields = $this->db->field_data( $table_name );
        }

        // There may be something wrong or the database driver may not return
        // field data
        if ( empty( $fields ) )
        {
            return false;
        }

        // Set option defaults
        $this->options['set_created']      = false;
        $this->options['set_modified']     = false;
        $this->options['use_soft_deletes'] = false;
        $this->options['log_user']         = false;

        // Check each field and set options
        foreach ( $fields as $field )
        {
            // Add field names to an array
            $this->options['field_names'][] = $field->name;

            // If field is a primary key, use it and protect the field.
            if ( ! empty( $field->primary_key ) && $field->primary_key == 1 )
            {
                $this->options['primary_key'] = $field->name;
                $this->options['protected'][] = $this->options['primary_key'];
            }

            switch ($field->name)
            {
                // If field is the 'created_on' field, protect the field and
                // enable 'set_created'.
                case $this->options['created_field'] :
                    $this->options['protected'][] = $this->options['created_field'];
                    $this->options['set_created'] = true;
                    break;

                // If field is the 'modified_on' field, protect the field and
                // enable 'set_modified'.
                case $this->options['modified_field'] :
                    $this->options['protected'][] = $this->options['modified_field'];
                    $this->options['set_modified'] = true;
                    break;

                // If field is the 'created_by' field, protect the field and
                // enable User Logging.
                case $this->options['created_by_field'] :
                    $this->options['protected'][] = $this->options['created_by_field'];
                    $this->options['log_user'] = true;
                    break;

                // If field is the 'modified_by' field, protect the field and
                // enable User Logging.
                case $this->options['modified_by_field'] :
                    $this->options['protected'][] = $this->options['modified_by_field'];
                    $this->options['log_user'] = true;
                    break;

                // If field is the 'deleted_by' field, protect the field and
                // enable User Logging.
                case $this->options['deleted_by_field'] :
                    $this->options['protected'][] = $this->options['deleted_by_field'];
                    $this->options['log_user'] = true;
                    break;

                // If field is the 'deleted' field, protect the field and
                // enable 'soft_deletes'.
                case $this->options['soft_delete_key'] :
                    $this->options['protected'][] = $this->options['soft_delete_key'];
                    $this->options['use_soft_deletes'] = true;
                    break;
            }
        }

        // Set our validation rules based on these fields
        $this->options['rules'] = $this->buildValidationRules( $fields );

        return $fields;
    }

    /**
     * Grabs the fields from the CLI options and gets them ready for
     * use within the views.
     */
    protected function parseFieldString($fields)
    {
        if ( empty( $fields ) )
        {
            return NULL;
        }

        $fields = explode( ' ', $fields );

        $new_fields = [ ];

        foreach ( $fields as $field )
        {
            $pop = [ NULL, NULL, NULL ];
            list( $field, $type, $size ) = array_merge( explode( ':', $field ), $pop );
            $type = strtolower( $type );

            $new_field = [
                'name'       => $field,
                'max_length' => $size
            ];

            // Strings
            if (in_array($type, ['char', 'varchar', 'string']))
            {
                $new_field['type']  = 'text';
            }

            // Textarea
            else if ($type == 'text')
            {
                $new_field['type'] = 'textarea';
            }

            // Number
            else if (in_array($type, ['tinyint', 'int', 'bigint', 'mediumint', 'float', 'double', 'number']))
            {
                $new_field['type'] = 'number';
            }

            // Date
            else if (in_array($type, ['date', 'datetime', 'time']))
            {
                $new_field['type'] = $type;
            }

            $new_fields[] = $new_field;
        }

        // Convert to objects
        array_walk($new_fields, function(&$item, $key) {
            $item = (object)$item;
        });

        return $new_fields;
    }

    /**
     * Takes the fields from field_data() and creates the basic validation
     * rules for those fields.
     *
     * @param array $fields
     *
     * @return array
     */
    public function buildValidationrules($fields=[])
    {
        if (empty($fields) || ! is_array($fields) || ! count($fields))
        {
            return null;
        }

        $rules = [];

        foreach ($fields as $field)
        {
            $rule = [];

            switch ($field->type)
            {
                // Numeric Types
                case 'tinyint':
                case 'smallint':
                case 'mediumint':
                case 'int':
                case 'integer':
                case 'bigint':
                    $rule[] = 'integer';
                    break;
                case 'decimal':
                case 'dec':
                case 'numeric':
                case 'fixed':
                    $rule[] = 'decimal';
                    break;
                case 'float':
                case 'double':
                    $rule[] = 'numeric';
                    break;

                // Date types don't have many defaults we can go off of...

                // Text Types
                case 'char':
                case 'varchar':
                case 'text':
                    $rule[] = 'alpha_numeric_spaces';
                    break;
            }

            if (! empty($field->max_length))
            {
                $rule[] = "max_length[{$field->max_length}]";
            }

            $rules[] = [
                'field' => $field->name,
                'label' => ucwords(str_replace('_', ' ', $field->name)),
                'rules' => implode('|', $rule)
            ];
        }

        $str = $this->stringify($rules);

        // Clean up the resulting array a bit.
        $str = substr_replace($str, "\n]", -3);

        return $str;
    }

}