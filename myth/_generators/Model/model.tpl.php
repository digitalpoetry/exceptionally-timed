<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Forge
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/*
 * Format option values
 */

// Format boolean values to strings
$set_created_string      = isset($set_created)      && $set_created ?      'true' : 'false';
$set_modified_string     = isset($set_modified)     && $set_modified ?     'true' : 'false';
$use_soft_deletes_string = isset($use_soft_deletes) && $use_soft_deletes ? 'true' : 'false';
$log_user_string         = isset($log_user)         && $log_user ?         'true' : 'false';
$return_insert_id_string = isset($return_insert_id) && $return_insert_id ? 'true' : 'false';
// Format arrays to strings
array_unshift($protected, 'submit');
$protected_string   = "\n\t\t'". implode("',\n\t\t'", $protected) . "'\n\t";
$field_names_string = "\n\t\t'". implode("',\n\t\t'", $field_names) . "'\n\t";

/*
 * Database Output.
 */

$database =
    "/**
     * @vstring \$table_name The model's default table name.
     */
    protected \$table_name = '{$table_name}';";
    
if ($primary_key !== 'id')
{
    $database .= empty($created_field) ? '' :
    "\n\n\t/**
     * @var string \$primary_key The model's default primary key.
     */
    protected \$primary_key = '{$primary_key}';";
}

/*
 * Auto-Date Output.
 */

// Set date_format, set_created
$auto_date =
    "/**
     * @var string \$date_format The type of date/time field used for
     *                          created_on' and 'modified_on' fields. Valid
     *                          types are: 'int', 'datetime', 'date'.
     */
    protected \$date_format = '{$date_format}';

    /**
     * @var boolean \$set_created Whether or not to auto-fill a 'created_on'
     *                           field on inserts.
     */
    protected \$set_created = {$set_created_string};";

// Set created_field
if ($set_created == true)
{
    $auto_date .= empty($created_field) ? '' :
    "\n\n\t/**
     * @var string \$created_field Field name to use as the 'created_on'
     *                            column in the DB table.
     */
    protected \$created_field = '{$created_field}';";
}

// Set set_modified
$auto_date .=
    "\n\n\t/**
     * @var boolean \$set_modified Whether or not to auto-fill a 'modified_on'
     *                           field on inserts.
     */
    protected \$set_modified = {$set_modified_string};";

// Set modified_field
if ($set_modified == true)
{
    $auto_date .= empty($modified_field) ? '' :
    "\n\n\t/**
     * @var string \$modified_field Field name to use as the 'modified_on'
     *                            column in the DB table.
     */
    protected \$modified_field = '{$modified_field}';";
}

/*
 * User Logging Output.
 */

// Set log_user
$user_logging =
    "/**
     * @var boolean \$log_user If true, will log user id for 'created_by',
     *                        'modified_by' and 'deleted_by'.
     */
    protected \$log_user = {$log_user_string};";
if ($log_user == true)
{
    // Set created_by_field
    $user_logging .= empty($created_by_field) ? '' :
    "\n\n\t/**
     * @var string \$created_by_field Field name to use to the 'created_by'
     *                               column in the DB table.
     */
    protected \$created_by_field = '{$created_by_field}';";
    // Set modified_by_field
    $user_logging .= empty($modified_by_field) ? '' :
    "\n\n\t/**
     * @var string \$modified_by_field Field name to use to the 'modified_by'
     *                                column in the DB table.
     */
    protected \$modified_by_field = '{$modified_by_field}';";
    // Set deleted_by_field
    $user_logging .= empty($deleted_by_field) ? '' :
    "\n\n\t/**
     * @var string \$deleted_by_field Field name to use to the 'deleted_by'
     *                               column in the DB table.
     */
    protected \$deleted_by_field = '{$deleted_by_field}';";
}

/*
 * Soft Deletes Output.
 */

// Set soft_deletes
$soft_deletes =
    "/**
     * @var boolean \$soft_deletes If true, will enable 'soft_deletes' for
     *                            the model.
     */
    protected \$soft_deletes = {$use_soft_deletes_string};";

if ($use_soft_deletes == true)
{
    // Set soft_delete_key
    $soft_deletes .= empty($soft_delete_key) ? '' :
    "\n\n\t/**
     * @var string \$soft_delete_key Field name to use as the 'deleted'
     *                              column in the DB table.
     */
    protected \$soft_delete_key = '{$soft_delete_key}';";
}

/**
 * Callback Methods.
 */

// Get callback option values
$callback_options = ['before_insert', 'after_insert', 'before_update', 'after_update', 'before_find', 'after_find', 'before_delete', 'after_delete'];
$methods = [ ];
foreach ($callback_options as $cb_option)
{
    $callbacks = $$cb_option;
    if ( empty($callbacks) )
    {
        continue;
    }
    // Loop callback names and set method info
    foreach (explode(' ', $callbacks) as $callback )
    {
        $name = ucfirst($callback);
        $desc = strstr($cb_option, 'before') ? 'Filter' : 'Hook';
        $methods[] = [
            'name'  => $name,
            'event' => $cb_option,
            'desc'  => $desc
        ];
    }
    // Wrap callback names in quotes, return a space if empty
    $$cb_option = empty($callbacks) ? ' ': "'" . $callbacks . "'";
}

// Add the method strings
$method_strings = [ ];
foreach ($methods as $method)
{
    $method_strings[] =
    "\n\t/**
     * {$method['name']} {$method['desc']}.
     *
     * Triggered on the '{$method['event']}' event.
     */
    protected function {$method['name']}()
    {
        // code..
    }\n";
}

// Wrap the methods string
$callback_methods = implode($method_strings);

/*
 * The Template
 */

echo "<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the CATT & CodeIgniter frameworks.
 *
 * Auto-generated by CATT on {$today}
 *
 * @package     DigitalPoetry\CATT\Model
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

use Myth\Models\CIDbModel;

/**
 * {$model_name} Model.
 */
class {$model_name} extends CIDbModel {

    /*
     * Database.
     */
    
    {$database}

    /*
     * Auto-Date Support.
     */
    
    {$auto_date}

    /*
     * User Logging.
     */
    
    {$user_logging}

    /*
     * Soft Deletes.
     */
    
    {$soft_deletes}

    /**
     * Event Hooks.
     */

    /**
     * @var array \$before_insert List of callback method names to hook onto the
     *                           'before_insert' event.
     */
    protected \$before_insert = [{$before_insert}];

    /**
     * @var array \$after_insert List of callback method names to hook onto the
     *                          'after_insert' event.
     */
    protected \$after_insert = [{$after_insert}];

    /**
     * @var array \$before_update List of callback method names to hook onto the
     *                           'before_update' event.
     */
    protected \$before_update = [{$before_update}];

    /**
     * @var array \$after_update List of callback method names to hook onto the
     *                          'after_update' event.
     */
    protected \$after_update = [{$after_update}];

    /**
     * @var array \$before_find List of callback method names to hook onto the
     *                         'before_find' event.
     */
    protected \$before_find = [{$before_find}];

    /**
     * @var array \$after_find List of callback method names to hook onto the
     *                        'after_find' event.
     */
    protected \$after_find = [{$after_find}];

    /**
     * @var array \$before_delete List of callback method names to hook onto the
     *                           'before_delete' event.
     */
    protected \$before_delete = [{$before_delete}];

    /**
     * @var array \$after_delete List of callback method names to hook onto the
     *                          'after_delete' event.
     */
    protected \$after_delete = [{$after_delete}];

    /**
     * Return Values.
     */

    /**
     * @var string \$return_type The type that items should be returned as.
     *                          Accepts 'array' or 'object'.
     */
    protected \$return_type = '{$return_type}';

    /**
     * @var boolean \$return_insert_id Whether to return item insert IDs.
     */
    protected \$return_insert_id = {$return_insert_id_string};

    /**
     * Fields.
     */

    /**
     * @var array \$protected_attributes Protected, non-modifiable attributes.
     */
    protected \$protected_attributes = [{$protected_string}];

    /**
     * @var array \$fields List of fields in the table.
     */
    protected \$fields = [{$field_names_string}];

    /**
     * @var array \$validation_rules Item field validation rules.
     */
    protected \$validation_rules = {$rules};


    /**
     * @var array \$insert_validate_rules Item field insert validation rules.
     */
    protected \$insert_validate_rules = [];

    /**
     * Methods.
     */

    /**
     * Initialize {$model_name} model.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Callbacks.
     */
    {$callback_methods}
}";
