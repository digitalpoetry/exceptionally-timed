<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Forge
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

$today = date('Y-m-d H:ia');

echo "<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Migration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Migration: Create API Log table.
 *
 * Created on: {$today}
 */
class Migration_Create_api_log_table extends CI_Migration {

    /**
     * Install the migration.
     *
     * @return void
     */
    public function up ()
    {
		\$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => '20',
				'null' => false,
				'unsigned' => true,
				'auto_increment' => true
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => true,         // To log unauthorized requests
				'unsigned' => true
			],
			'ip_address' => [
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => false
			],
			'duration' => [
				'type' => 'FLOAT',
				'null' => false
			],
			'request' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => false
			],
			'method' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => false
			],
			'created_on' => [
				'type' => 'DATETIME',
				'null' => false
			]
		];
		\$this->dbforge->add_field(\$fields);
		\$this->dbforge->add_key('id', true);
		\$this->dbforge->add_key('user_id');

		\$this->dbforge->create_table('api_logs', true, config_item('migration_create_table_attr'));
    }

    /**
     * Uninstall the migration.
     *
     * @return void
     */
    public function down ()
    {
		\$this->dbforge->drop_column('users', 'api_key');
    }

}";
