<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Forge
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

$today = date('Y-m-d H:ia');

echo "<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Migration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Migration: Add api_key to user table.
 *
 * Created on: {$today}
 */
class Migration_Add_digest_key_to_users extends CI_Migration {

    /**
     * Install the migration.
     *
     * @return void
     */
    public function up ()
    {
		\$field = [
			'digest_key' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => true
			]
		];
		\$this->dbforge->add_column('users', \$field);
    }

    /**
     * Uninstall the migration.
     *
     * @return void
     */
    public function down ()
    {
		\$this->dbforge->drop_column('users', 'digest_key');
    }

}";
