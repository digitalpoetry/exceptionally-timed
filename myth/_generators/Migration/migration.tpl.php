<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Forge
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

$up     = '';
$down   = '';

/*
 * Create
 */
if ($action == 'create')
{
    $up  = "\$fields = {$fields};";
    $up .= "\n\t\t\$this->dbforge->add_field(\$fields);";
    if (! empty($primary_key))
    {
        $up .= "\n\t\t\$this->dbforge->add_key('{$primary_key}', true);";
    }
    $up .="\n\t\t\$this->dbforge->create_table('{$table}', true, config_item('migration_create_table_attr') );";

    $down = "\$this->dbforge->drop_table('{$table}');";
}

/*
 * Add
 */
if ($action == 'add' && ! empty($column))
{
    $up  = "\$field = {$column_string};";
    $up .= "\n\t\t\$this->dbforge->add_column('{$table}', \$field);";

    $down = "\$this->dbforge->drop_column('{$table}', '{$column}');";
}

/*
 * Remove
 */
if ($action == 'remove' && ! empty($column))
{
    $up = "\$this->dbforge->drop_column('{$table}', '{$column}');";

    $down  = "\$field = {$column_string};";
    $down .= "\n\t\t\$this->dbforge->add_column('{$table}', \$field);";
}

/**
 * The Template
 */
echo "<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Database\Migration
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

/**
 * Migration: {$clean_name}
 *
 * Created on: {$today}
 *
 * @property \$dbforge
 */
class Migration_{$name} extends CI_Migration {

    /**
     * Runs when migration is installed.
     *
     * @return void
     */
    public function up ()
    {
        {$up}
    }

    /**
     * Runs when migration is uninstalled.
     *
     * @return void
     */
    public function down ()
    {
        {$down}
    }

}";
