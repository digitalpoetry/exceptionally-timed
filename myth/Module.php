<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

namespace Myth;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Module Library.
 *
 * Provides a base class for module definition files to extend from.
 *
 * @uses IN_ADMIN Constant indicating if in the admin area.
 */
class Module {

	/**
	 * @var object $ci The CodeIgniter instance.
	 */
	protected $ci;

	/**
	 * @var string $active_module The current module.
	 */
	protected $active_module = false;

	/**
	 * Initialize the Module class.
	 *
	 * @return void
	 */
	public function __construct()
	{
	    // Get the CodeIgniter instance
	    $this->ci =& get_instance();

		// Set the active module name if not set
		if (! empty($this->name))
		{
			$this->active_module = $this->name;
		}

		// Run the module initialization method if it is defined
		if (defined('IN_ADMIN'))
		{
			$this->doInitAdmin();
		}
		else
		{
			$this->doInit();
		}
	}

	/**
	 * Runs initialization methods, like hooking into the menus, tapping
	 * into hooks, etc.
	 *
	 * @return void
	 */
	public function doInit()
	{
		if (method_exists($this, 'init'))
		{
			$this->init();
		}
	}

	/**
	 * Runs initialization methods, like hooking into the menus, tapping
	 * into hooks, etc. for Admin area
	 *
	 * @return void
	 */
	public function doInitAdmin()
	{
		if (method_exists($this, 'initAdmin'))
		{
			$this->initAdmin();
		}
	}

}