<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

namespace Myth\Navigation;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MenuCollection Library.
 *
 * Provides a simple collection of menus that is accessible from any other file.
 */
class MenuCollection {

	/**
	 * @var array $menus The collection of menus.
	 */
	protected static $menus = [];

	/**
	 * Returns a menu object identified by $name. If one doesn't already
	 * exist, then it will create a new one.
	 *
	 * @param  string $name The menu name(slug).
	 * @return object       The menu for the given name.
	 */
	public static function menu($name)
	{
		$name = strtolower($name);

		if (! array_key_exists($name, self::$menus))
		{
			self::$menus[$name] = new Menu($name);
		}

		return self::$menus[$name];
	}

	/**
	 * Removes a single menu from the collection.
	 *
	 * @param  string $name The name of the menu to delete.
	 * @return void
	 */
	public function deleteMenu($name)
	{
	    $name = strtolower($name);

		if (array_key_exists($name, self::$menus))
		{
			unset(self::$menus[$name]);
		}
	}

}