<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

namespace Myth\Navigation;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MenuItem Library.
 *
 * Provides a class to make it easier working with menu items.
 */
class MenuItem {

    /**
     * @var string $name The name(slug) of this item.
     */
    protected $name = null;

    /**
     * @var string $title The Title set for this item.
     */
    protected $title = null;

    /**
     * @var string $link The link for this item.
     */
    protected $link = null;

    /**
     * @var string $icon The icon for this item.
     */
	protected $icon = null;

    /**
     * @var array $children The submenu items for this item.
     */
    protected $children = null;

    /**
     * @var string $attributes The attributes for this item.
     */
    protected $attributes = null;

    /**
     * @var integer $order The menu position priorty for this item.
     */
    protected $order = 0;

    /**
     * Creates the menu item and sets item properties.
     * 
     * @return void
     */
    public function __construct($name, $title=null, $link=null, $icon=null, $order=0)
    {
        $this->name  = $this->prepareName($name);
        $this->title = is_null($title) ? ucwords($this->name) : $title;
        $this->link  = $link;
	    $this->icon  = $icon;
	    $this->order = $order;
    }

    /**
     * Returns the name(slug) of this item.
     *
     * @return string|null The name for this item or the default null.
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * Returns the Title set for this item.
     *
     * @return string|null The title for this item or the default null.
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * Returns the link for this item.
     *
     * @return string|null The link for this item or the default null.
     */
    public function link()
    {
        return $this->link;
    }

	/**
	 * Returns the icon for this item.
	 *
     * @return string|null The icon for this item or the default null.
	 */
	public function icon()
	{
	    return $this->icon;
	}


    /*
     * Attributes
     */

    /**
     * Retrieves an existing attribute.
     *
     * @param  string      $name The name of the attribute to retrieve.
     * @return string|null       The value of the attribute or null if not found.
     */
    public function attribute($name)
    {
        if (! is_string($name))
        {
            return false;
        }

        $name = strtolower($name);

        return isset($this->attributes[$name]) ? $this->attributes[$name] : NULL;
    }

    /**
     * Returns the raw attributes as set.
     *
     * @return array|null The menu item raw attributes.
     */
    public function attributes()
    {
        return $this->attributes;
    }

    /**
     * Sets an attribute for this item. If an existing attribute of this
     * name exists, will overwrite the value.
     *
     * @param  string $name  The name of the attribute to set.
     * @param  string $value Optional. The value of the attribute. Default is an
     *                       empty string.
     * @return object        The menu item object.
     */
    public function setAttribute($name, $value='')
    {
        if (! is_string($name))
        {
            return false;
        }

        $this->attributes[ strtolower($name) ] = $value;

        return $this;
    }

    /**
     * Adds a new value to an attribute. If the attribute isn't set yet,
     * it adds it. If it is set, it creates an array and merges the two.
     *
     * @param  string $name  The name of the attribute to merge.
     * @param  string $value Optional. The value of the attribute. Default is an
     *                       empty string.
     * @return object        The menu item object.
     */
    public function mergeAttribute($name, $value='')
    {
        if (! is_string($name))
        {
            return false;
        }

        $name = strtolower($name);

        // If the attribute isn't set, then we'll create it as a string.
        if (! isset($this->attributes[$name]))
        {
            return $this->setAttribute($name, $value);
        }

        // If it does exist, and isn't an array already, we'll need to make it one
        if (isset($this->attributes[$name]) && ! is_array($this->attributes[$name]))
        {
            $this->attributes[$name] = [ $this->attributes[$name] ];
        }

        // Merge our value in
        $this->attributes[$name][] = $value;

        return $this;
    }

    /**
     * Removes an attribute.
     *
     * @param  str         $name The name of the attribute to remove.
     * @return object|bool       The menu item object or false if `$name` is not
     *                           a string.
     */
    public function unsetAttribute($name)
    {
        if (! is_string($name))
        {
            return false;
        }

        $name = strtolower($name);

        unset($this->attributes[$name]);

        return $this;
    }


    /*
     * Order
     */

    /**
     * Sets the order the item should appear in the collection.
     * Will often be reset by the Menu sorting.
     *
     * @param  integer $order Optional. The desired position of the menu item. 
     *                        Default is 0.
     * @return void
     */
    public function setOrder($order = 0)
    {
        $this->order = (int)$order;
    }

    /**
     * Returns the menu item position.
     * 
     * @return integer The position of the menu item.
     */
    public function order()
    {
        return (int)$this->order;
    }


    /*
     * Children
     */

    /**
     * Returns the raw list of child items.
     *
     * @return array|null The raw array of child items or null is not set.
     */
    public function children()
    {
        return $this->children;
    }

    /**
     * Adds a child menu item to `$this` item. Allows us to create groups of
     * links within a menu.
     *
     * @see MenuItem The menu item class.
     *
     * @param  object $item The child menu item.
     * @return object       The parent menu item.
     */
    public function addChild(MenuItem $item)
    {
        if (! is_array($this->children))
        {
            $this->children = [];
        }

        $this->children[] = $item;

        return $this;
    }

    /**
     * Finds and returns a child with a matching name(slug).
     *
     * @param  string      $name The name of child menu item.
     * @return object|null       The child menu item, or null if not found.
     */
    public function childNamed($name)
    {
        $name = $this->prepareName($name);

        foreach ($this->children as $child)
        {
            if ($child->name() == $name)
            {
                return $child;
            }
        }

        return NULL;
    }

    /**
     * Checks if a menu item has a submenu.
     *
     * @return boolean Returns true if there is a submenu.
     */
    public function hasChildren()
    {
        return (bool)count($this->children);
    }

    /**
     * Private Methods
     */

    /**
     * Sanitizes a menu item name(slug).
     *
     * @param  string $name The menu item name.
     * @return string       The sanitized name string.
     */
    private function prepareName($name)
    {
        return strtolower( strip_tags($name) );
    }

}