<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

namespace Myth\Navigation;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Menu Library.
 *
 * Provides a class to make it easier working with menus.
 */
class Menu {

    /**
     * @var string $attributes The html attributes of the menu.
     */
    protected $attributes = '';

    /**
     * @var string|null $name The name(slug) of the menu.
     */
    protected $name = null;

    /**
     * @var array $items The child items of this menu. Note that each item can
     *            contain more items for nested menus.
     */
    protected $items = [];

    /**
     * Creates a new menu.
     *
     * @param  string $name The name of the menu.
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $this->prepareName($name);
    }

    /**
     * Returns the name of the menu.
     * 
     * @return string|null The menu name(slug) or the default null.
     */
    public function name()
    {
        return $this->name;
    }


    /*
     * Items
     */

    /**
     * Returns the raw array of items.
     *
     * @return array The raw array of items.
     */
    public function items()
    {
        return $this->items;
    }

    /**
     * Adds a MenuItem to our list of items. This could be either an instance of
     * MenuItem or a associative array of a menu item.
     *
     * @see MenuItem The menu item class.
     *
     * @param  object $item The item object or array.
     * @return object       The updated menu.
     */
    public function addItem(MenuItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Retrieves an existing menu item.
     * 
     * @param  string      $name The name of the item.
     * @return object|void       The item or void if not found.
     */
    public function itemNamed($name)
    {
        $name = $this->prepareName($name);

        foreach ($this->items as $item)
        {
            if ($item->name() == $name)
            {
                return $item;
            }
        }
    }

	/**
	 * A convenience method to create a new item if one with that
	 * name doesn't already exist.
     *
     * @see MenuItem The menu item class.
	 *
     * @param  object $item The item object or associative array.
     * @return void
	 */
	public function ensureItem(MenuItem $item)
	{
	    if (! $this->itemNamed( $item->name() ))
	    {
		    $this->addItem($item);
	    }
	}

    /**
     * Determines whether any items exist or not.
     *
     * @return boolean Returns true if menu has items, false if not.
     */
    public function hasItems()
    {
        return (bool)count($this->items);
    }

    /**
     * Adds a child menu item to an existing parent item. If the parent
     * item doesn't exist, one will be created.
     *
     * @see MenuItem The menu item class.
     *
     * @param  object $child  The child item.
     * @param  string $parent The parent item name.
     * @return object         The updated menu.
     */
    public function addChild(MenuItem $child, $parent)
    {
        // If no parent item exists, create one.
        $parent_item = $this->itemNamed($parent);

        if (! $parent_item)
        {
            $this->addItem( new MenuItem($parent) );
            $parent_item = $this->itemNamed($parent);
        }

        $parent_item->addChild($child);

        return $this;
    }


    /*
     * Sorting
     */

    /**
     * Sorts items by order, name or title in ascending or decending direction.
     * 
     * @param  string $key The key to sort by. Accepts 'order', 'name' or 'title'.
     * @param  string $dir Optional. The sort direction. Accepts 'asc' or
     *                     'desc'. Default is 'asc'.
     * @return object      The sorted menu.
     */
    public function sortBy($key, $dir='asc')
    {
        $method = $key .'_sorter';

        if (! method_exists($this, $method))
        {
            throw new \RuntimeException('Unable to sort by method: '. $key);
        }

        $this->items = $this->{$method}($this->items);

        if ($dir == 'desc')
        {
            $this->items = array_reverse($this->items);
        }

        return $this;
    }


    /*
     * Private Methods
     */

    /**
     * Helper method to cleanup our name to a common format.
     *
     * @param  string $name The name.
     * @return string       The modifed name.
     */
    private function prepareName($name)
    {
        return strtolower( strip_tags($name) );
    }

    /**
     * Sorts menu item by order.
     * 
     * @param  array $items The items.
     * @return array        The sorted items.
     */
    private function order_sorter($items)
    {
        usort($items, function ($a, $b)
            {
                return $a->order() - $b->order();
            }
        );

        return $items;
    }

    /**
     * Sorts menu item by name.
     * 
     * @param  array $items The items.
     * @return array        The sorted items.
     */
    private function name_sorter($items)
    {
        usort($items, function ($a, $b)
            {
                return strnatcmp($a->name(), $b->name() );
            }
        );

        return $items;
    }

    /**
     * Sorts menu item by title.
     * 
     * @param  array $items The items.
     * @return array        The sorted items.
     */
    private function title_sorter($items)
    {
        usort($items, function ($a, $b)
            {
                return strnatcmp($a->title(), $b->title() );
            }
        );

        return $items;
    }

}