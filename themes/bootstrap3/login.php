<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><!-- Header -->
<?= $themer->display('bootstrap:blocks/header') ?>

<style>
    body {
        padding-bottom: 40px;
        background-color: #eee;
    }
    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto 40px auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }
    .form-signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input {
        margin-bottom: 10px;
    }
    .pass-strength {
        min-height: 1.5em;
    }
</style>

<!-- Content -->
<div class="container content">

    <!-- Form Signin -->
    <div class="form-signin">
        <?= $view_content ?>
    </div><!-- /.form-signin -->

</div><!-- /.content -->

<!-- Footer -->
<?= $themer->display('bootstrap:blocks/footer') ?>
