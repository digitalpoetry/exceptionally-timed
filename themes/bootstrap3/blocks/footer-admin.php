<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?>  <!-- Footer -->
    <footer class="footer">
        <hr />
        <div class="container-fluid text-right">
            <span class="text-muted small">
                Page rendered in {elapsed_time} seconds using {memory_usage}.
            </span>
        </div>
    </footer><!-- /#footer -->

    <!-- Scripts -->
    <script src="<?= site_url('themes/bootstrap3/js/jquery-min.js') ?>"></script>
    <script src="<?= site_url('themes/bootstrap3/js/bootstrap-min.js') ?>"></script>
    <script src="<?= site_url('assets/js/ajax.js') ?>"></script>
    <?php foreach ($external_scripts as $script) : ?>
        <script src="<?= $script ?>"></script>
    <?php endforeach; ?>

    <!-- Loading Spinner -->
    <div id="ajax-loader" class="alert-warning">Loading...</div>

</body>
</html>