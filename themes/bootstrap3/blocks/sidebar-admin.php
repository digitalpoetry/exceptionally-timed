<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><aside class="left-off-canvas-menu">
    <?php
    if (isset($menu_sidenav))
    {
        $ul_opened = false;
        foreach ($menu_sidenav as $item)
        {
            if ( empty($item->link()) )
            {
                if ($ul_opened)
                {
                    echo '</ul>';
                }

                $ul_opened = false;

                echo '<h3>', $item->title(), '</h3>';
            }
            else
            {
                if ( ! $ul_opened )
                {
                    echo '<ul class="nav nav-pills nav-stacked nav-sidebar">';
                }

                $ul_opened = true;

                echo '<li><a href="', $item->link(), '">', $item->title(), '</a></li>';
            }
        }
    }
    ?>
</aside><!-- /.left-off-canvas-menu -->