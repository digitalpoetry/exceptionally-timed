<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><header class="navbar navbar-inverse navbar-fixed-top" id="navbar" role="banner">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Toggle -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button><!-- /.navbar-toggle -->
            
            <!-- Brand -->
            <a class="navbar-brand" href="<?= site_url() ?>">
                <img height="40" src="<?= site_url('assets/images/catt.png') ?>">
                <?= config_item('site.name') ?>
            </a><!-- /.navbar-brand -->

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">

            <!-- Main Menu -->
            <!--<ul id="main-menu" class="nav navbar-nav navbar-left">-->
                <?php // if (isset($menu_topnav)) bootstrap_menu($menu_topnav) ?>
            <!--</ul>--><!-- /#main-menu -->

            <!-- User Menu -->
            <ul id="user-menu" class="nav navbar-nav navbar-right">
                <?php if ( $is_logged_in ) : ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" ><span class="glyphicon glyphicon-user"></span> <?= ucwords($username) ?></a>
                    <ul class="dropdown-menu">
                        <?php if (isset($menu_usernav)) bootstrap_menu($menu_usernav) ?>
                        <li><a href="<?= site_url( Myth\Route::named('logout') ) ?>" onclick="return confirm('Logout?');">Logout</a></li>
                    </ul>
                </li>
                <?php else : ?>
                <li><a href="<?= site_url( Myth\Route::named('login') ) ?>">Login</a></li>
                <?php endif; ?>
            </ul><!-- /#user-menu -->

        </div>
    </div>
</header><!-- /.navbar -->