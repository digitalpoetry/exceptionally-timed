<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><!DOCTYPE html>
<html lang="en">
<head>

    <!-- Metatags -->
    <?= $html_meta->renderTags() ?>

    <!-- Favicon -->
    <link rel="icon" href="<?= site_url('assets/images/favicon.ico') ?>">

    <!-- Title -->
    <title><?= config_item('site.name') ?></title>

    <!-- Styles -->
    <?= link_tag('themes/bootstrap3/css/theme-simplex.css') ?>
    <?= link_tag('themes/bootstrap3/css/site.css') ?>
    <?= link_tag('themes/bootstrap3/css/dashboard.css') ?>
    <?php foreach ($stylesheets as $style) : ?>
    <?= link_tag($style) ?>
    <?php endforeach ?>
    
    <!-- HTML5 shim & Respond.js -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="admin">
    
    <!-- Navbar -->
    <?= $themer->display('bootstrap:blocks/navbar-admin') ?>