<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?>  <!-- Footer -->
    <footer class="footer" id="footer">
        <div class="<?= $containerClass ?> text-right">
            <span class="text-muted small">
                Page rendered in {elapsed_time} seconds using {memory_usage}.
            </span>
        </div>
    </footer><!-- /#footer -->

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <?php foreach ($external_scripts as $script) : ?>
        <script src="<?= $script ?>"></script>
    <?php endforeach; ?>

</body>
</html>