<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><!-- Header -->
<?= $themer->display('bootstrap:blocks/header-admin') ?>

<!-- Content -->
<div class="container-fluid">

    <div class="row">

        <!-- Sidebar -->
        <div class="col-sm-3 col-md-2 sidebar">
            <?= $themer->display('bootstrap:blocks/sidebar-admin') ?>
        </div>

        <!-- Main Content -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <?= $notice ?>
            <?= $view_content ?>
            <?= $themer->display('bootstrap:blocks/footer-admin') ?>
        </div>

    </div>

</div><!-- /.container -->

