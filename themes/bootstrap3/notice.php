<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><?php if (isset($notice) && ! empty($notice)) : ?>

    <!-- Notice -->
    <div class="alert alert-<?= $notice['type'] ?> alert-dismissible" id="notice" role="alert">
        <button type="button" class="close" data-dismiss="alert">
        	<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        </button>
        <?= $notice['message'] ?>
    </div><!-- /#notice -->

<?php else: ?>

    <!-- Notice -->
    <div id="notice"></div>

<?php endif; ?>
