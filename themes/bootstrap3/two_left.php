<?php
/**
 * Code All The Things!
 *
 * Project jumpstarter based on the Sprint & CodeIgniter frameworks.
 *
 * @package     DigitalPoetry\CATT\Theme\Bootstrap
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz Code All The Things!
 * @version     0.1.0 Shiny Things
 * @filesource  
 */

?><!-- Header -->
<?= $themer->display('bootstrap:blocks/header') ?>

<!-- Content -->
<div class="content <?= $containerClass ?>">
    <div class="row">

        <!-- Sidebar -->
        <div class="col-sm-3 col-md-2" id="sidebar">
            <?= $themer->display('bootstrap:blocks/sidebar') ?>
        </div><!-- /#sidebar -->

        <!-- Main -->
        <div class="col-sm-9 col-md-10" id="main">
            <?= $notice ?>
            <?= $view_content ?>
        </div><!-- /#main -->

    </div>
</div><!-- /.content -->

<!-- Footer -->
<?= $themer->display('bootstrap:blocks/footer') ?>
